#include "Calculate.h"


double calculate::ReturnRad(int x1, int y1, int x2, int y2){
	double a = ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));//マウスと基準点の距離の二乗
	double b = (x1 - x2) * (x1 - x2);//マウス座標と基準点のX座標の長さを出す
	double c = sqrt(b) / sqrt(a);//マウス座標と基準点のcos値を出す/*平方根なしだと10度前後ズレる*/
	if (x2 > x1 && y2 > y1){
		return acos(c);
	}
	else
	if (x2 < x1 && y2 > y1){
		return PI - acos(c);
	}
	else
	if (x2 > x1 && y2 < y1){
		return  2 * PI - acos(c);
	}
	else
	if (x2 < x1 && y2 < y1){
		return acos(c) + (3 / 2)*PI;
	}
	else
	if (y2 == y1 && x2 > x1){//プログラム上で0度
		return 0;
	}
	else
	if (y2 > y1 && x2 == x1){//プログラム上で90度
		return PI / 2;
	}
	else
	if (y2 == y1 && x2 < x1){//プログラム上で180度
		return PI;
	}
	else
	if (y2 < y1 && x2 == x1){//プログラム上で270度
		return PI * (1.5);
	}
}


int calculate::HittoLinandCircle(double a, double b, double c, double d, double e, double f, double g){
	float inner;//内積
	float PQ_Vector_X, PQ_Vector_Y;//pqベクトル
	float PM_Vector_X, PM_Vector_Y;//pdベクトル
	float PQ_Vector2, PM_Vector2;//ベクトルの二乗
	/*Barのベクトル*/
	PQ_Vector_X = a - c;
	PQ_Vector_Y = b - d;
	/*バーとボールのベクトル*/
	PM_Vector_X = a - e;
	PM_Vector_Y = b - f;

	inner = PQ_Vector_X * PM_Vector_X + PQ_Vector_Y * PM_Vector_Y;//PQ PM の内積
	/*以下ベクトルの二乗の形の算出*/
	PQ_Vector2 = PQ_Vector_X * PQ_Vector_X + PQ_Vector_Y * PQ_Vector_Y;
	PM_Vector2 = PM_Vector_X * PM_Vector_X + PM_Vector_Y * PM_Vector_Y;

	/*K値が一以上0以下になると数値上おかしいので弾く*/
	float k = inner / PQ_Vector2;
	if (!(k>1 || k<0)){
		float ph2 = (inner*inner) / PQ_Vector2;//phの二乗
		float mh2 = PM_Vector2 - ph2;//mhの二乗を三平方の定理から求める
		if (mh2 <= g * g){
			//DrawFormatString(100, 100, GetColor(125, 125, 125), "OKOKOKOKOKOKOKOKOKOK");
			return 1;
		}
	}

	return 0;
}


int calculate::HittoBoxandCircle(double Tx, double Ty, double Tsizex, double Tsizey, double Cx, double Cy, double Cr){
	//n += HittoLinandCircle(Tx - Tsizex / 2, Ty - Tsizey / 2, Tx + Tsizex / 2, Ty + Tsizey / 2, Cx, Cy, Cr);
	/*n += HittoLinandCircle(Tx - Tsizex / 2, Ty - Tsizey / 2, Tx + Tsizex / 2, Ty - Tsizey / 2, Cx, Cy, Cr);
	n += HittoLinandCircle(Tx + Tsizex / 2, Ty - Tsizey / 2, Tx + Tsizex / 2, Ty + Tsizey / 2, Cx, Cy, Cr);
	n += HittoLinandCircle(Tx + Tsizex / 2, Ty + Tsizey / 2, Tx - Tsizex / 2, Ty + Tsizey / 2, Cx, Cy, Cr);
	n += HittoLinandCircle(Tx - Tsizex / 2, Ty + Tsizey / 2, Tx - Tsizex / 2, Ty - Tsizey / 2, Cx, Cy, Cr);
	*/
	double vertex_x[4], vertex_y[4];
	vertex_x[0] = Tx + Tsizex / 2;
	vertex_y[0] = Ty + Tsizey / 2;
	vertex_x[1] = Tx - Tsizex / 2;
	vertex_y[1] = Ty + Tsizey / 2;
	vertex_x[2] = Tx - Tsizex / 2;
	vertex_y[2] = Ty - Tsizey / 2;
	vertex_x[3] = Tx + Tsizex / 2;
	vertex_y[3] = Ty - Tsizey / 2;

	//}
	int n = 0;
	n += HittoLinandCircle(vertex_x[0], vertex_y[0], vertex_x[1], vertex_y[1], Cx, Cy, Cr / 2);
	n += HittoLinandCircle(vertex_x[1], vertex_y[1], vertex_x[2], vertex_y[2], Cx, Cy, Cr / 2);
	n += HittoLinandCircle(vertex_x[2], vertex_y[2], vertex_x[3], vertex_y[3], Cx, Cy, Cr / 2);
	n += HittoLinandCircle(vertex_x[3], vertex_y[3], vertex_x[0], vertex_y[0], Cx, Cy, Cr / 2);
	if (n > 0){
		return 1;
	}
	else{
		return 0;
	}

	return 0;
}

int calculate::HittoCircleandCircle(double x1, double y1, double r1, double x2, double y2, double r2){
	double length = ( x2 - x1 )*( x2 - x1 ) + ( y2 - y1 )*( y2 - y1 );
	double L2 = (r1 + r2)*(r1 + r2);
	if (length <= L2)
		return 1;
	else
		return 0;
}

double calculate::returnQuarterNearRad(double f){
	double x = cos(f);
	double y = sin(f);
	double r =	1.0f/sqrt(2.0f);

	if ( r <= x && x <= 1.0f && -r <= y && y <= 0.0f)return 0*PI/2;
	else
	if (r <= x && x <= 1.0f && 0.0f <= y && y <= r)return 0 * PI / 2;
	else
	if (x <= r && -r <= x && r <= y && y <= 1.0f)return PI / 2;
	else
	if (-1.0f <= x && x <= -r && y <= r && -r <= y)return 2 * PI / 2;
	else
	if (-r <= x && x <= r && -1.0f <= y && y <= -r)return 3 * PI / 2;

	return 0.0f;
}