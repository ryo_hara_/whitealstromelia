#ifndef _STAGE_H_
#define _STAGE_H_

#include <List>
#include <vector>
#include "Mover.h"
#include "Player.h"
#include "Load.h"
#include"InputMngr.h"
#include "Calculate.h"
#include "Music.h"
#include "DarkChenge.h"

#define CAMERARANGE_X 656
#define CAMERARANGE_Y 480


//32*1.5 = 48
enum SceanType{
	Type_Flame,
	Type_Title,
	Type_StartLoading,
	Type_StartTalkScean,
	Type_Stage1_1,
	Type_Stage1_1to2TalkScean,
	Type_Stage1_2,
	Type_Stage1to2TalkScean,
	Type_Stage2_FirsttalkScean,
	Type_Stage2_1,
	Type_Stage2_1to2TalkScean,
	Type_Stage2_2,
	Type_Stage2LastTalkScean,
	Type_Stage2to3TalkScean,
	Type_Stage3_1,
	Type_Stage3_2,
	Type_Stage3_3,
	Type_TrialVerFinishScean,
	Type_GameOver,
};


enum NowState{
	State_Start,
	State_Wait,
	State_Update,
	State_NextSceanAhead,
	State_GameOver,
};


/*シーン系のクラス*/
class scean{
private:
	bool update_flag;				//このシーンの更新を行うかのフラグ
	bool scean_finish_flag;			//このシーンを終わらせるかの判断をするフラグ
	int SceanType;					//シーンの種類を格納するフラグ変数
	int nowstate;					//現在の状態(待機中、次に進む)を格納する変数
public:

	scean(){
		update_flag = true;
		scean_finish_flag = false;
		SceanType = -1;
		nowstate = State_Update;
		SetNowState(State_Start);
	};
	~scean(){};

	DarkChange DC;//暗転処理関数の委譲
	InputMngr	*IM = InputMngr::getInstance();

	virtual void update(){};		//シーンで行う描画、計算処理を記述
	int SetScean();					//シーン開始時のシーン内のオブジェクト生成処理を記述
	virtual void UpdateList(){};

	void Debug(){ if (IM->key[DEBUG_P] % 10 == 1)
		SetNowState(State_NextSceanAhead); };

	int RetrunSceanType(){ return SceanType; }
	int SetSceanType(int _Type){ SceanType = _Type; return _Type; };

	void SetNowState(int state){ nowstate = state;}
	int  ReturnNowState(){ return nowstate; }
	bool ReturnUpdateFlag(){ return update_flag; };
	bool SetUpdateFlag(bool a){ update_flag = a; return update_flag; };
	bool ReturnFinishFlag(){ return scean_finish_flag; };
	bool SetFinishFlag(bool a){ scean_finish_flag = a; return scean_finish_flag; };
};




/*Stage系シーンクラス*/
class StageScean : public scean{
private:
	int CameraX, CameraY;
	int MapXNum, MapYNum;
	int **Map;
	int MapLengthX, MapLengthY;
	double Magnification;
public:
	StageScean(){
		Magnification = MAGNIFICATION;
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
	}

	std::vector<Enemy*> Enemy_List;
	std::vector<Player*> Player_List;
	std::vector<Bullet*> Bullet_List;
	std::vector<MapObj*> MapObj_List;
	std::vector<Effect*> Effect_List;

	LoadGraphic *LG =	LoadGraphic::getInstance();	//LoadGraphの画像を取得するためのインスタンス(シングルトンクラスのため複数のインスタンスを生成しても内容は同一)
	PlayerMngr	*PM =	PlayerMngr::getInstance();
	LoadSound	*LM =	LoadSound::getInstance();
	MusicMngr	*MMngr = MusicMngr::getInstance();

	calculate Cal;

	//void Debug();

	int DeleteMap(){free(Map);return 0;};
	int DeleteObj();
	int SetMap(char *c);
	int SetMapObj();
	int SetCamera();
	void update();
	void UpdateList();
	void Draw();
	void CameraUpdate();

	void PlayerAttackPart();
	void EnemyAttckPart();
	void SetEnemyAttack();

	void HitDicision();

	void getPlayerItem();

	virtual void StageEvent(){
	};

	int HittoMapObjandMover();
	int HittoMapObjandBullet();

};




class Stage1_1 : public StageScean{
public:
	Stage1_1(){
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		SetNowState(State_Start);
		SetSceanType(Type_Stage1_1);
		SetMap("Data\\Map\\Stage1_1.txt");
		Player_List.push_back(new Player1());
	};
	~Stage1_1(){
		DeleteMap();
	};
	void StageEvent();
};


class Stage1_2 : public StageScean{
public:
	Stage1_2(){
		SetSceanType(Type_Stage1_2);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		SetMap("Data\\Map\\Stage1_2.txt");
		SetNowState(State_Start);

		Player_List.push_back(new Player1());
	};
	~Stage1_2(){
		DeleteMap();
	};
	void StageEvent();
};


class Stage2_1 : public StageScean{
public:
	Stage2_1(){
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		SetNowState(State_Start);
		SetSceanType(Type_Stage2_1);
		SetMap("Data\\Map\\Stage2_1.txt");
		Player_List.push_back(new Player1());
	};
	~Stage2_1(){
		DeleteMap();
	};
	void StageEvent();
};

class Stage2_2 : public StageScean{
public:
	Stage2_2(){
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		SetNowState(State_Start);
		SetSceanType(Type_Stage2_2);
		SetMap("Data\\Map\\Stage2_2.txt");
		Player_List.push_back(new Player1());
	};
	~Stage2_2(){
		DeleteMap();
	};
	void StageEvent();
};



#endif