
#include "Behavior.h"
#include <stdlib.h>
#include <time.h>

#define MOVEDIST 1//�ړ�����
#define MOVEDIST_FAST 3

void independentPattern::move(double &x, double &y, double &vector_x, double &vector_y, int count, int ChangeInterval){
	/*
	��:0
	��:1
	��:2
	�E:3
	*/

	if (count == 0)movePatternCounter = 0;
	else
	if (count % ChangeInterval == 0){
		srand((unsigned int)time(NULL));
		movePatternCounter = rand() % 4;
	}
	if (count % ChangeInterval < 20 ){
		if (movePatternCounter == 0){
			y += -MOVEDIST;
			vector_x = 0;
			vector_y = -MOVEDIST;
		}
		else
		if (movePatternCounter == 1){
			y += MOVEDIST;
			vector_x = 0;
			vector_y = +MOVEDIST;
		}
		else
		if (movePatternCounter == 2){
			x -= MOVEDIST;
			vector_x = -MOVEDIST;
			vector_y = 0;
		}
		else
		if (movePatternCounter == 3){
			x += MOVEDIST;
			vector_x = +MOVEDIST;
			vector_y = 0;
		}
	}
	return;
}

void SpeedyindependentPattern::move(double &x, double &y, double &vector_x, double &vector_y, int count, int ChangeInterval){
	static int a = 1;
	if (count == 0)a = 0;
	else
	if (count % ChangeInterval == 0){
		srand((unsigned int)time(NULL));
		a = rand() % 4;
	}
	if (count % ChangeInterval < 20){
		if (a == 0){
			y += -MOVEDIST_FAST;
			vector_x = 0;
			vector_y = -MOVEDIST_FAST;
		}
		else
		if (a == 1){
			y += MOVEDIST_FAST;
			vector_x = 0;
			vector_y = +MOVEDIST_FAST;
		}
		else
		if (a == 2){
			x -= MOVEDIST_FAST;
			vector_x = -MOVEDIST_FAST;
			vector_y = 0;
		}
		else
		if (a == 3){
			x += MOVEDIST_FAST;
			vector_x = +MOVEDIST_FAST;
			vector_y = 0;
		}
	}
	return;
}
