#include "Talk.h"


void Stage1_1to2TalkScean::update(){
	TalkSkip();
	talk();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		IM->SetInputFlag(true);
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetFinishFlag(true);
		}
	}

}

void Stage1_1to2TalkScean::talk(){
	Draw();
	static int Char1Dim = 135;
	static int Char2Dim = 0;
	static int Char3Dim = 0;
	//背景
	SetDrawMode(DX_DRAWMODE_BILINEAR);
	DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, GRASSLAND), TRUE); 

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char1Dim);
	DrawRotaGraph(150, 600, 1.0f, 0.0f, LG->ReturnGraph(TypeStandingGraph, BRAVE), TRUE);//Char1
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char2Dim);
	DrawRotaGraph(650, 600, 0.95f, 0.0f, LG->ReturnGraph(TypeStandingGraph, ARCHER), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char3Dim);
	DrawRotaGraph(400, 600, 0.90f, 0.0f, LG->ReturnGraph(TypeStandingGraph, WITCH), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);


	SetDrawMode(DX_DRAWMODE_BILINEAR);//線形補間関数
	//テキストボックス
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 200);
	DrawBox(50, 450, 750, 575, GetColor(0, 0, 0), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);


	if (ReturnTalkPoint() == 37){
		SetNowState(State_NextSceanAhead);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
	}


	switch (ReturnTalkPoint()){
	case 0:
		SetFontSize(32);
		Char1Dim = 135;
		Char2Dim = 135;
		Char3Dim = 135;
		SetFontSize(32);
		DrawString(70, 510, "−草原−", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 1:
		SetFontSize(24);
		DrawString(70, 470, "　無事に王都を出発した勇者一行は魔王の城を目指し草原を", GetColor(235, 235, 235));
		DrawString(70, 500, "進んでいた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 2:
		DrawString(70, 470, "「・・・・・・疲れた」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 3:
		DrawString(70, 470, "「まだ半分しか進んでないんだけど」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 4:
		DrawString(70, 470, "「もう半分もだよー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 5:
		DrawString(70, 470, "「こんなんで根を上げてどうするんだよ、まだ先は長いぞ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 6:
		DrawString(70, 470, "「今日はどこまで進む予定なんだ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 7:
		DrawString(70, 470, "「取りあえずここを抜けて砂漠の手前までは行こうと」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 8:
		DrawString(70, 470, "「まぁそんなものか」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 9:
		DrawString(70, 470, "「妥当なところだろ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 10:
		DrawString(70, 470, "「長いよー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 11:
		DrawString(70, 470, "「砂漠に入らないだけまだマシだろ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 12:
		DrawString(70, 470, "「飯はどうするんだ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 13:
		DrawString(70, 470, "「草原抜けてからだね」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 14:
		DrawString(70, 470, "「えー！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 15:
		DrawString(70, 470, "「それは些か厳しんじゃないか」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 16:
		DrawString(70, 470, "「何とかなるでしょ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 17:
		DrawString(70, 470, "「私にいい考えがある」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 18:
		DrawString(70, 470, "「一応聞こう」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 19:
		DrawString(70, 470, "「（これ駄目なやつだよなぁ）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 20:
		DrawString(70, 470, "「帰って飯食って寝るのさ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 21:
		DrawString(70, 470, "「・・・・・・ん？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 22:
		DrawString(70, 470, "「成程、名案だな」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 23:
		DrawString(70, 470, "「でしょー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 24:
		DrawString(70, 470, "「ワンモアプリーズミー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 25:
		DrawString(70, 470, "「疲れたから帰って寝る！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 26:
		DrawString(70, 470, "「帰りません！！第一アゲート！！何でお前まで賛成してん", GetColor(235, 235, 235));
		DrawString(70, 500, "  だよ！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 27:
		DrawString(70, 470, "「何か面倒くさくなってしまってなぁ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 28:
		DrawString(70, 470, "「ローズも何考えてるんだよ！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 29:
		DrawString(70, 470, "「だって疲れたんだもんー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 30:
		DrawString(70, 470, "「諦めろって、後半分進んだらそこで休む予定だから」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 31:
		DrawString(70, 470, "「ぶーぶー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 32:
		DrawString(70, 470, "「まぁ仕方ないか」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 33:
		DrawString(70, 470, "「ほらさっさと行くぞ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 34:
		SetFontSize(24);
		DrawString(70, 470, "「BooBoo」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 35:
		DrawString(70, 470, "「いつまで言ってんだよ」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 36:
		DrawString(70, 470, "再び進み始める勇者一行、こんな調子で大丈夫なのだろうか", GetColor(235, 235, 235));
		DrawString(70, 500, "？　尚、似たようなやり取りが後数回繰り返されたようであ", GetColor(235, 235, 235));
		DrawString(70, 530, "る", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	}


	if (IM->key[ATTACK] % 10 == 1)AddTalkPoint();


	switch (ReturnTalkChar()){
	case Talk_Null:
		Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char1:
		Char1Dim = 255;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char2:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char3:
		if (Char1Dim != 0)Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		Char3Dim = 255;
		break;
	case Talk_Char2_3:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		Char3Dim = 255;
		break;
	}
	return;

}

void Stage1_1to2TalkScean::Draw(){
}