#ifndef _CALCULATE_H_
#define _CALCULATE_H_
#include <math.h>
#define PI 3.14159265

class calculate{//計算用関数群をまとめたクラス
public:
	double	ReturnRad(int x1, int y1, int x2, int y2);/*（x1,y1）は敵の座標　、（x2,y2）は自機の座標　*/
	int		HittoLinandCircle(double a, double b, double c, double d, double e, double f, double g);
	int		HittoBoxandCircle(double Tx , double Ty , double Tsizex , double Tsizey , double Cx,double Cy  , double Cr);
	int		HittoCircleandCircle(double x1 ,double y1 , double r1 , double x2 , double y2 , double r2);
	double  returnQuarterNearRad(double f);//引数の変数から0,90,180,270の中で近い値を返す
};
#endif