#include "SpecialScean.h"
#include "Game.h"

#define DIST 5.0


void Title::draw(){
	int c = LG->ReturnGraph(TypeTitle, TMainGraph_1);
	DrawRotaGraph(WINDOWSIZEX / 2, WINDOWSIZEY / 2, 1.0f, 0.0f, c,TRUE);
	int y;
	if (nowCursol % 2 == Cursol_GameStart) y = 235;
	else y = 320;

	DrawRotaGraph(105 + DIST*cos(CursolObj), y + 25, 1.0f, 0.0f, LG->ReturnGraph(TypeTitle, StartCursol), TRUE);
	//DrawBox(80 + DIST*cos(CursolObj), y, 130 + DIST*cos(CursolObj), y+50, GetColor(135, 135, 135), TRUE);
	
}

void Title::update(){
	draw();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetNowState(State_Update);
			MMngr->PushMusic(TITLE_BGM, TRUE);
		}
	}else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		if (IM->key[ATTACK] > 0 && nowCursol % 2 == Cursol_GameStart){
			SetNowState(State_NextSceanAhead);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
			MMngr->PushEffectSound(DECISION_SOUND_1);
		}
		if (IM->key[ATTACK] > 0 && nowCursol % 2 == Cursol_Exit)exit(0);
		CursolObj += PI / 30;
		if (IM->key[DOWN] % 10 == 1){
			nowCursol++;
			MMngr->PushEffectSound(CURSOL_SOUND_1);
		}
		else
		if (IM->key[UP] % 10 == 1){
			nowCursol--;
			MMngr->PushEffectSound(CURSOL_SOUND_1);
		}
	}else
	if(ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}


}

void Title::UpdateList(){

}