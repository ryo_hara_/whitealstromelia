#include "Talk.h"


void Stage2_LastTalkScean::Draw(){
}

void Stage2_LastTalkScean::update(){
	TalkSkip();
	talk();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		IM->SetInputFlag(true);
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetFinishFlag(true);
		}
	}

}

void Stage2_LastTalkScean::talk(){
	Draw();
	static int Char1Dim = 135;
	static int Char2Dim = 0;
	static int Char3Dim = 0;
	//背景
	SetDrawMode(DX_DRAWMODE_BILINEAR);

	if (ReturnTalkPoint() >= 46 && 50 >= ReturnTalkPoint()){
		DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, DESERT), TRUE);
	}
	else{
		DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, DESERT), TRUE);
	}

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char1Dim);
	DrawRotaGraph(150, 600, 1.0f, 0.0f, LG->ReturnGraph(TypeStandingGraph, BRAVE), TRUE);//Char1
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char2Dim);
	DrawRotaGraph(650, 600, 0.95f, 0.0f, LG->ReturnGraph(TypeStandingGraph, ARCHER), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char3Dim);
	DrawRotaGraph(400, 600, 0.90f, 0.0f, LG->ReturnGraph(TypeStandingGraph, WITCH), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);


	SetDrawMode(DX_DRAWMODE_BILINEAR);//線形補間関数
	//テキストボックス
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 200);
	DrawBox(50, 450, 750, 575, GetColor(0, 0, 0), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);

	if (ReturnTalkPoint() == 10){
		SetNowState(State_NextSceanAhead);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
	}

	//１　ラズ
	//2　アゲート

	switch (ReturnTalkPoint()){
	case 0:
		Char1Dim = 135;
		Char2Dim = 0;
		Char3Dim = 0;
		SetFontSize(32);
		DrawString(70, 510, "−砂漠−", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 1:
		SetFontSize(24);
		DrawString(70, 470, "　熱砂の大地を進んでいた勇者一行の前に現れたのは視界", GetColor(235, 235, 235));
		DrawString(70, 500, "に収まりきらないほどの大きな街、いや都とでも言うべき巨", GetColor(235, 235, 235));
		DrawString(70, 530, "大なもので、中央に一際大きな城が建っており、それを中心", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 2:
		DrawString(70, 470, "として広がっていた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 3:
		DrawString(70, 470, "「思いのほか疲れたな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 4:
		DrawString(70, 470, "「にしても大きいなこれ、下手したら王都より大きいんじゃ", GetColor(235, 235, 235));
		DrawString(70, 500, "ない？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 5:
		DrawString(70, 470, "「確かにそうかもしれんな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 6:
		DrawString(70, 470, "「もう、駄目。早いとこ中に入って休もうよー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 7:
		DrawString(70, 470, "「それもそうだね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 8:
		DrawString(70, 470, "「だがそう簡単に入れるものなのだろうか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 9:
		DrawString(70, 470, "「まぁ行けば分かるでしょ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 10:
		break;
		/*
	case 10:
		DrawString(70, 470, "数分後", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
		*/
	case 11:
		DrawString(70, 470, "「意外とすんなり入れたことに驚きを隠せない。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 12:
		DrawString(70, 470, "「入れたんだから別にいーじゃんそんなこと。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 13:
		DrawString(70, 470, "「門番さん驚いてたね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 14:
		DrawString(70, 470, "「ああ、態々砂漠を通ってきた事にな。」ローズ「あーあ、", GetColor(235, 235, 235));
		DrawString(70, 500, "最初から転移的なことで来れるって知ってたらもっと楽でき", GetColor(235, 235, 235));
		DrawString(70, 530, "たのになー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 15:
		DrawString(70, 470, "「何だよ、無事に着けたからいいだろ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 16:
		DrawString(70, 470, "「それにしても魔物が多いな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 17:
		DrawString(70, 470, "「そりゃそうだ、俺たちでいう王都みたいな所なんだから。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 18:
		DrawString(70, 470, "「でもさぁ、よくよく見ると結構人もいるよね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 19:
		DrawString(70, 470, "「簡単に来れるんだから遊びに来てるんだろう。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 20:
		DrawString(70, 470, "「これからどーすの？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 21:
		DrawString(70, 470, "「とりあえず宿でもとって休もう。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 22:
		DrawString(70, 470, "「飯はどうする？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 23:
		DrawString(70, 470, "「どこか店にでも行って食べようか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 24:
		DrawString(70, 470, "「さんせー、何か美味しものあるかなぁ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 25:
		DrawString(70, 470, "「とにかく宿探そうぜ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 26:
		DrawString(70, 470, "―時間経過中―", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 27:
		DrawString(70, 470, "すっかりと日が落ち、昼間とはまた違った賑わいを見せる都", GetColor(235, 235, 235));
		DrawString(70, 500, "の一角、飲食店が並ぶ通りにある店に入り食事をとっていた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 28:
		DrawString(70, 470, "「にしても、どんな料理が出てくるかと思ったら案外ふつー", GetColor(235, 235, 235));
		DrawString(70, 500, "だね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 29:
		DrawString(70, 470, "「どんなのを想像してたんだよ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 30:
		DrawString(70, 470, "「こう、もうちょっとグロテスクでネバネバ的な？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 31:
		DrawString(70, 470, "「そんな物を食うやつはいないだろ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 32:
		DrawString(70, 470, "「わかんないよー。もしかしたらいるかもよ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 33:
		DrawString(70, 470, "「どうでもいいが、この料理は旨いな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 34:
		DrawString(70, 470, "「確かにねぇ、とっても美味しいよね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 35:
		DrawString(70, 470, "「当たりだったみたいだな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 36:
		DrawString(70, 470, "「そうだな、結構な数のお客さんも入ってるしな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 37:
		DrawString(70, 470, "「それで、明日はどうするのさ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 38:
		DrawString(70, 470, "「明日ねぇ、魔王城に行って、魔王に会う。以上！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 39:
		DrawString(70, 470, "「常々馬鹿だとは思っていたが・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 40:
		DrawString(70, 470, "「正真正銘のだねー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 41:
		DrawString(70, 470, "「何だよ！！実際そう言うほかないだろ！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 42:
		DrawString(70, 470, "「だからと言って、もう少し言い方があっただろ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 43:
		DrawString(70, 470, "「まぁ、シンプルで分かりやすくはあるけどね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 44:
		DrawString(70, 470, "「で、試練という事でやってきた訳だが後は何が待ち受けて", GetColor(235, 235, 235));
		DrawString(70, 500, "いるんだか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 45:
		DrawString(70, 470, "「さあ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 46:
		DrawString(70, 470, "「あれだよきっと、魔王と直接対決とかでしょ。お約束的に", GetColor(235, 235, 235));
		DrawString(70, 500, "。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 47:
		DrawString(70, 470, "「いや待って、そうなったら絶対負けるんだけど！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 48:
		DrawString(70, 470, "「まぁそうだな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 49:
		DrawString(70, 470, "「勝てるわけないしねー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 50:
		DrawString(70, 470, "「何それ！？二人とも何か軽くない！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 51:
		DrawString(70, 470, "「いやまぁ、応援はしてるけど、魔王には会ったことないし", GetColor(235, 235, 235));
		DrawString(70, 500, "。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 52:
		DrawString(70, 470, "「別にお前の恋が破れたところで特に何て影響はないからな", GetColor(235, 235, 235));
		DrawString(70, 500, "。応援はしてるが。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 53:
		DrawString(70, 470, "「お前ら薄情すぎないか！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 54:
		DrawString(70, 470, "「だから応援はしてるって。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 55:
		DrawString(70, 470, "「応援はな。まぁそうと決まったわけではないし、明日にな", GetColor(235, 235, 235));
		DrawString(70, 500, "ればわかることだ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 56:
		DrawString(70, 470, "「そうだよな！！まだ決まったわけじゃないもんな！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 57:
		DrawString(70, 470, "「ああ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 58:
		DrawString(70, 470, "「（わかりやすいフラグだなー。これ絶対バトルなパターン", GetColor(235, 235, 235));
		DrawString(70, 500, "だよねぇ。お約束的にも。）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 59:
		DrawString(70, 470, "「さぁ、明日は頑張るぞー！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 60:
		DrawString(70, 470, "「「おー！！」」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2_3);
		break;
	}


	if (IM->key[ATTACK] % 10 == 1)AddTalkPoint();


	switch (ReturnTalkChar()){
	case Talk_Null:
		Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char1:
		Char1Dim = 255;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char2:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char3:
		if (Char1Dim != 0)Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		Char3Dim = 255;
		break;
	case Talk_Char2_3:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		Char3Dim = 255;
		break;
	}
}