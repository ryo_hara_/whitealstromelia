#include "Talk.h"


void Stage2_1to2TalkScean::Draw(){
}

void Stage2_1to2TalkScean::update(){
	TalkSkip();
	talk();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		IM->SetInputFlag(true);
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetFinishFlag(true);
		}
	}

}

void Stage2_1to2TalkScean::talk(){
	Draw();
	static int Char1Dim = 135;
	static int Char2Dim = 0;
	static int Char3Dim = 0;
	//背景
	SetDrawMode(DX_DRAWMODE_BILINEAR);

	if (ReturnTalkPoint() >= 46 && 50 >= ReturnTalkPoint()){
		DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, DESERT), TRUE);
	}
	else{
		DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, DESERT), TRUE);
	}

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char1Dim);
	DrawRotaGraph(150, 600, 1.0f, 0.0f, LG->ReturnGraph(TypeStandingGraph, BRAVE), TRUE);//Char1
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char2Dim);
	DrawRotaGraph(650, 600, 0.95f, 0.0f, LG->ReturnGraph(TypeStandingGraph, ARCHER), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char3Dim);
	DrawRotaGraph(400, 600, 0.90f, 0.0f, LG->ReturnGraph(TypeStandingGraph, WITCH), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);


	SetDrawMode(DX_DRAWMODE_BILINEAR);//線形補間関数
	//テキストボックス
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 200);
	DrawBox(50, 450, 750, 575, GetColor(0, 0, 0), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);

	if (ReturnTalkPoint() == 39){
		SetNowState(State_NextSceanAhead);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
	}

	//１　ラズ
	//2　アゲート

	switch (ReturnTalkPoint()){
	case 0:
		Char1Dim = 135;
		Char2Dim = 0;
		Char3Dim = 0;
		SetFontSize(32);
		DrawString(70, 510, "−砂漠−", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 1:
		SetFontSize(24);
		DrawString(70, 470, "容赦なく照り付ける太陽、周りは一面不毛の大地、行けども", GetColor(235, 235, 235));
		DrawString(70, 500, "行けども同じ景色。誰しもが思い描く砂漠を勇者一行は進ん", GetColor(235, 235, 235));
		DrawString(70, 530, "でいた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 2:
		DrawString(70, 470, "「暑い・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 3:
		DrawString(70, 470, "「暑いな・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 4:
		DrawString(70, 470, "「暑いねぇ・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 5:
		DrawString(70, 470, "「これ何処まで続いているんだ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 6:
		DrawString(70, 470, "「そんなの俺が知るわけないだろう。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 7:
		DrawString(70, 470, "「何かもう溶けちゃいそう。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;	
	case 8:
		DrawString(70, 470, "暑いというよりも、もはや熱いという砂漠の過酷な環境は確", GetColor(235, 235, 235));
		DrawString(70, 500, "かに勇者一行の体力を奪いつつあった。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 9:
		DrawString(70, 470, "「さっき会った魔物さんたちも暑さでバテてたねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 10:
		DrawString(70, 470, "訂正しよう体力を奪われていたのは勇者一行だけではなかっ", GetColor(235, 235, 235));
		DrawString(70, 500, "たらしい。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 11:
		DrawString(70, 470, "「マジで！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 12:
		DrawString(70, 470, "「うん、何か倒れて運ばれてたよ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 13:
		DrawString(70, 470, "「これだけ暑いんだ仕方あるまい。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 14:
		DrawString(70, 470, "「でさぁ、一つ思ったんだけど魔物さんたちって自分たちの", GetColor(235, 235, 235));
		DrawString(70, 500, "町から私達の方に来る時ってこの砂漠を通ることになるんで", GetColor(235, 235, 235));
		DrawString(70, 530, "しょ？それって大変だよねー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 15:
		DrawString(70, 470, "「確かにそうだな、その都度ここを通るとは・・・・・・。", GetColor(235, 235, 235));
		DrawString(70, 500, "」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 16:
		DrawString(70, 470, "「二人とも何言ってるのさ、こんな所わざわざ通ってるわけ", GetColor(235, 235, 235));
		DrawString(70, 500, "ないじゃん。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 17:
		DrawString(70, 470, "「・・・・・・ん？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2_3);
		break;

	case 18:
		DrawString(70, 470, "「何かこう、魔法的な？転移的なサムシングで移動している", GetColor(235, 235, 235));
		DrawString(70, 500, "らしいよ。魔王が言ってた。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 19:
		DrawString(70, 470, "「そんなこと聞いてないよー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;


	case 20:
		DrawString(70, 470, "「同じく、聞いてないぞ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 21:
		DrawString(70, 470, "「そりゃ聞かれなかったからね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;


	case 22:
		DrawString(70, 470, "「そんな便利なものがあったら初めから使えばよかったじゃ", GetColor(235, 235, 235));
		DrawString(70, 500, "ないかぁ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;


	case 23:
		DrawString(70, 470, "「まったくだ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;


	case 24:
		DrawString(70, 470, "「いや、だって、ほら試練らしいし？こう、いきなり魔王と", GetColor(235, 235, 235));
		DrawString(70, 500, "ご対面ってのも心の準備が、ね？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 25:
		DrawString(70, 470, "「「ね？」じゃねーよ、乙女じゃないんだからよぉ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 26:
		DrawString(70, 470, "「何か魔物の皆さんにすごく申し訳ない気持ちになってきた", GetColor(235, 235, 235));
		DrawString(70, 500, "よ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 27:
		DrawString(70, 470, "「確かに、この暑い中ずっと待機してもらってたんだよな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 28:
		DrawString(70, 470, "「言われてみれば確かにそーだな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 29:
		DrawString(70, 470, "「言われてみればじゃないよ、まったく・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 30:
		DrawString(70, 470, "「・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 31:
		DrawString(70, 470, "「取りあえずさっさとこの砂漠を抜けるぞ。彼らが早いとこ", GetColor(235, 235, 235));
		DrawString(70, 500, "休めるように。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 32:
		DrawString(70, 470, "「そーだねー。私たちも疲れたし。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 33:
		DrawString(70, 470, "「・・・・・・なぁ何かほら、目的が違くない？普通そこは", GetColor(235, 235, 235));
		DrawString(70, 500, "俺のために〜とかさぁ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 34:
		DrawString(70, 470, "「さっさと行くぞー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 35:
		DrawString(70, 470, "「後で魔物さんたちにもお礼言っとこうか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 36:
		DrawString(70, 470, "「そーだな、まったく魔王も何を考えてるんだか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 37:
		DrawString(70, 470, "「・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 38:
		DrawString(70, 470, "今一度、決心を新たにした勇者一行（約一名を除く）。果た", GetColor(235, 235, 235));
		DrawString(70, 500, "して無事に砂漠を突破することはできるのだろうか？", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	}


	if (IM->key[ATTACK] % 10 == 1)AddTalkPoint();


	switch (ReturnTalkChar()){
	case Talk_Null:
		Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char1:
		Char1Dim = 255;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char2:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char3:
		if (Char1Dim != 0)Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		Char3Dim = 255;
		break;
	case Talk_Char2_3:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		Char3Dim = 255;
		break;
	}
}