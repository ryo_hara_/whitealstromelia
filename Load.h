#ifndef _LOAD_H_
#define _LOAD_H_
#include "DxLib.h"
/*ロードクラスに登録させる画像の種類の数*/
enum RegisterGraphicNum{
	MAXMAPCHIP		= 50,
	PCHARAGRAPH		= 50,
	TITLEGRAPH		= 10,
	ENEMYGRAPH		= 50,
	EFFECTGRAPH		= 100,
	BULLETGRAPH		= 50,
	UIGRAPH			= 100,
	STANDINGFRAPH	= 50,
	BACKSCREANGRAPH = 50,
	SCEANGRAPH		= 50
};

enum GraphicType{
	TypeMapChip			= 0,
	TypePlayerChara		= 1,
	TypeEnemy			= 2,
	TypeTitle			= 3,
	TypeEffect			= 4,
	TypeBullet			= 5,
	TypeUI				= 6,
	TypeStandingGraph	= 7,
	TypeBackScreanGraph = 8, //背景
	TypeSceanGraph		= 9, //ゲームオーバー画面とか諸々
};


//画像ロード用のクラス
class LoadGraphic{
private:
	LoadGraphic(){};
	static LoadGraphic* loadgraph;
	~LoadGraphic(){};
	int EnemyCharaGraph[ENEMYGRAPH];
	int PCharaGraph[PCHARAGRAPH];
	int MapChip[MAXMAPCHIP];
	int TitleGraph[TITLEGRAPH];
	int EffectGraph[EFFECTGRAPH];
	int BulletGraph[BULLETGRAPH];
	int SceanGraph[SCEANGRAPH];//シーンで扱うオブジェクトint;
	int UIGraph[UIGRAPH];
	int StandingCharaGraph[STANDINGFRAPH];
	int BackScreanGraph[BACKSCREANGRAPH];
public:
	static LoadGraphic* getInstance(){ return loadgraph; }
	
	int DeletAllGraph(void);

	int ReturnGraph(int Type,int Allocate);
	int LoadCharaStandingGraph(void);
	int LoadUI(void);
	int LoadTitle(void);
	int LoadEnemy(void);
	int LoadEffect(void);
	int LoadStage(void);
	int LoadPlayerChara(void);
	int LoadBullet(void);
	int LoadBackScrean(void);
	int LoadSceanGraph(void);
};

enum CharaStandingGraphicAllocation{
	BRAVE	= 0,
	ARCHER	= 5,
	WITCH	= 10,
	DEVIL   = 15,
};


enum UIGraphicAllocation{
	NUMERAL_TYPE1	= 0,//0~9までを内在
	MULTIPLY_TYPE1	= 10,
	LIFEHERT		= 11,
	LIFEFLAME		,
};


/*マップチップ格納変数の配列番号に割り当てる画像の設定*/
enum MapChipGraphAllocation{
	GRASS = 0,
	ROCK = 1,
	BLOCK = 2,
	WORP_B = 3,
	WORP_G = 4,
	WORP_P = 5,
	KEY_1 = 6,
	KEY_2 = 7,
	ITEM = 8,
	FENCE = 9,
	DOOR_1 = 10,
	Kari = 14,
	DESERTCHIP,
	ROCK_RED,
};

enum TitleGraphicAllocation{
	TMainGraph_0 = 0,
	TMainGraph_1,
	StartCursol
};

enum PCharaGraphicAllocation{
	TESTHERO	= 0,
	HERO_1 = 5,
	HERO_2 = 10,
	HERO_3 = 20,
	HERO_4 = 30,
	HERO_5 = 40
};

enum EnemyGraphicAllocation{/*敵の画像の配置を記録した定数*/
	BAT = 0,
	BIRD = 5,
	DRAGON = 10,
	GHOST = 15,
	GOLEM  = 20,
	RABBIT = 25,
	SLIME = 30
};


enum EffectGraphAllocation{
	Worp_B_Effect = 0,
	Worp_G_Effect = 5,
	Worp_P_Effect = 10,
	Expload_Blue  = 15
};

enum EffectGraphNum{/*エフェクトの枚数を格納した定数*/
	WORP_GRAPH_NUM = 4 
};

enum BulletGraphAllocation{
	RICE_B = 0,
	SHOT_HERO_1 = 1,
	RABBIT_BULLET,
	BAT_BULLET,
	BIRD_BULLET,
};

enum BackScreanGraphAllocation{
	FOUNTAIN,	//噴水
	GRASSLAND,	//草原
	WITCHHOUSE,	//魔女の家
	DESERT
};

enum SceanGraphAllocation{
	GAMECLEAR,
	GAMECLEAR_S1,
	GAMEOVER,
	GAMEOVER_S1,
};




//音楽ロード用のクラス
class LoadSound{
private:
	LoadSound(){};
	static LoadSound* loadsound;
	~LoadSound(){};

	int BGM[100];
	int EffectSound[100];

public:
	static LoadSound* getInstance(){ return loadsound; }
	int DeletAllSound();
	int LoadBGM();
	int LoadEffectSound();
	int ReturnSound(int Allocate);
	int ReturnEffectSound(int Allocate);
	//void LoadSound(){}
};


enum MusicAllocat{
	TITLE_BGM = 0,
	TALK_BGM_1,
	STAGE_1_BGM,
	GAMEOVER_BGM,
	CLEAR_BGM
};

enum EffectSoundAllocat{
	HIT_SOUND_1,
	ATTACK_SOUND_1,
	PLAYER1_ATTACKSOUND,
	ENEMY_HITSOUND_1,
	CURSOL_SOUND_1,
	DECISION_SOUND_1
};


#endif