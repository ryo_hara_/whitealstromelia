#include"Stage.h"
#include"MapObj.h"
#include"Enemy.h"
#include "Effect.h"
#include "Bullet.h"
#include<math.h>
#define PI 3.14159265

int CheckCrossVector(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int &returnx, int &returny);


enum PlayerItem{
	ITEM_KEY_1
};


enum MapChipType{
	MCTYPE_ROCK = 1,
	MCTYPE_WORP_B = 6,
	MCTYPE_WORP_G = 7,
	MCTYPE_WORP_P = 8,
	MCTYPE_PLAYER = 9,
	MCTYPE_ITEM	= 10,
	MCTYPE_SLIME	= 15,
	MCTYPE_RABBIT = 16,
	MCTYPE_BIRD	= 17,
	MCTYPE_BAT	= 18,
	MCTYPE_GHOST	= 19,
	MCTYPE_GOLEM	= 20,
	MCTYPE_DRAGON = 21,
	MCTYPE_DOOR_1 = 22,
	MCTYPE_KEY_1	= 23,
	/*
	0	:無し
	1	:岩
	2	:
	3	:
	4	:
	5	:
	6	:ワープ青
	7	:ワープ緑
	8	:ワープ紫
	9	:プレイヤー
	10	:アイテム
	11	:
	12	:
	13	:
	14	:
	15	:スライム
	16	:うさぎ
	17	:鳥
	18	:コウモリ
	19	:ゴースト
	20	:ゴーレム
	21	:ドラゴン
	22	:柵
	23	:鍵
	*/
};


//void StageScean::HitDecision


int StageScean::DeleteObj(){
	for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end();){
		if ((*itr)->ReturnFlag() == FALSE) {
			delete(*itr);
			itr = MapObj_List.erase(itr);
			continue;
		}
		itr++;
	}
	/*
	for (std::list<Player*>::iterator itr = Player_List.begin(); itr != Player_List.end(); ){
		if ((*itr)->ReturnFlag() == FALSE) {
			itr = Player_List.erase(itr);
			continue;
		}
		itr++;
	}*/
	for (std::vector<Effect*>::iterator itr = Effect_List.begin(); itr != Effect_List.end();){
		if ((*itr)->ReturnFlag() == FALSE) {
			delete (*itr);
			itr = Effect_List.erase(itr);
			continue;
		}
		itr++;
	}
	for (std::vector<Enemy*>::iterator itr = Enemy_List.begin(); itr != Enemy_List.end();){
		if ((*itr)->ReturnFlag() == FALSE) {
			Effect_List.push_back(new ExploadEffect((*itr)->x,(*itr)->y));
			delete (*itr);
			itr = Enemy_List.erase(itr);
			continue;
		}
		itr++;
	}
	for (std::vector<Bullet*>::iterator itr = Bullet_List.begin(); itr != Bullet_List.end();){
		if ((*itr)->ReturnFlag() == FALSE) {
			delete (*itr);
			itr = Bullet_List.erase(itr);
			continue;
		}
		itr++;
	}
	return 0;
}



void StageScean::CameraUpdate(){
	int Rx = CAMERARANGE_X/2;
	int	Ry = CAMERARANGE_Y/2;
	if (PM->x < Rx){
		CameraX = -MAPSTARTX ;
	}else
	if (PM->x > MapLengthX-Rx)
	{
		CameraX = MapLengthX - Rx * 2 - MAPSTARTX;
	}else{
		CameraX = PM->x - MAPSTARTX - Rx;
	}

	if (PM->y < Ry){
		CameraY = -MAPSTARTY;
	}else 
	if (PM->y > MapLengthY-Ry){
		CameraY = MapLengthY - Ry * 2 - MAPSTARTY;

	}else{
		CameraY = PM->y - MAPSTARTY - Ry;
	}
	return;
}

int StageScean::SetCamera(){
	CameraX = MAPSTARTX - 450;
	CameraY = MAPSTARTY ;
	return 0;
}

int StageScean::SetMap(char *c){
	char c_[1000];
	FILE *f;
	if ((f = fopen(c, "r")) == NULL){
		exit(1);
	}
	/*マップの縦横のマス数を取得*/
	fgets(c_, 5, f);
	MapXNum = atoi(c_);
	fgets(c_, 5, f);
	MapYNum = atoi(c_);
	/*動的に二次元配列を確保*/
	Map = (int**)malloc(sizeof(int)* MapYNum);
	for (int n = 0; n < MapYNum; n++)Map[n] = (int *)malloc(sizeof(int)* MapXNum);
	/*各マスに割り振られた数値を取得*/
	char *ss = " ,";
	char *tok;
	for (int y = 0; y < MapYNum; y++){
		fgets(c_, 1000, f);
		tok = strtok(c_, ss);
		for (int x = 0; x < MapXNum; x++){
			Map[y][x] = atoi(tok);
			tok = strtok(NULL, ss);
			if (tok == NULL)
				break;
		}
	}
	/*マップの縦横の長さを格納*/
	MapLengthX = (int)((MapXNum+1) * MAPOBJECTSIZE);
	MapLengthY = (int)(MapYNum * MAPOBJECTSIZE);
	fclose(f);
	SetMapObj();
	SetCamera();
	return 0;
}


int StageScean::SetMapObj(){
	int graph__;
	if (RetrunSceanType() == Type_Stage2_1 || RetrunSceanType() == Type_Stage2_2)
		graph__ = LG->ReturnGraph(TypeMapChip,ROCK_RED);
	else
		graph__ = LG->ReturnGraph(TypeMapChip, ROCK);


	for (int y = 0; y < MapYNum; y++){
		for (int x = 0; x < MapXNum; x++){
			int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
			int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
			if (Map[y][x] == MCTYPE_ROCK){
				int x_ =  (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ =  (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				MapObj_List.push_back(new Rock( x_ , y_ , graph__ ) );
			}else 
			if (Map[y][x] == MCTYPE_PLAYER){
				PM->x = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				PM->y = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
			}else
			if (Map[y][x] == MCTYPE_SLIME){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Slime(x_, y_, LG->ReturnGraph(TypeEnemy, SLIME)));
			}else
			if (Map[y][x] == MCTYPE_RABBIT){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Rabbit(x_, y_, LG->ReturnGraph(TypeEnemy, RABBIT)));
			}else
			if (Map[y][x] == MCTYPE_BIRD){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Bird(x_, y_, LG->ReturnGraph(TypeEnemy, BIRD)));
			}else
			if (Map[y][x] == MCTYPE_BAT){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Bat(x_, y_, LG->ReturnGraph(TypeEnemy, BAT)));
			}else
			if (Map[y][x] == MCTYPE_GHOST){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Ghost(x_, y_, LG->ReturnGraph(TypeEnemy, GHOST)));
			}else
			if (Map[y][x] == MCTYPE_GOLEM){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Golem(x_, y_, LG->ReturnGraph(TypeEnemy, GOLEM)));
			}else
			if (Map[y][x] == MCTYPE_DRAGON){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				Enemy_List.push_back(new Dragon(x_, y_, LG->ReturnGraph(TypeEnemy, DRAGON)));
			}else
			if (Map[y][x] == MCTYPE_WORP_B){
				MapObj_List.push_back(new WorpPoint(x_, y_, LG->ReturnGraph(TypeMapChip, WORP_B)));
				Effect_List.push_back(new WorpEffect(x_, y_, LG->ReturnGraph(TypeEffect, Worp_B_Effect)));
			}
			else
			if (Map[y][x] == MCTYPE_WORP_G){
				MapObj_List.push_back(new WorpPoint(x_, y_, LG->ReturnGraph(TypeMapChip, WORP_G)));
				Effect_List.push_back(new WorpEffect(x_, y_, LG->ReturnGraph(TypeEffect, Worp_G_Effect)));
			}
			else
			if (Map[y][x] == MCTYPE_WORP_P){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				MapObj_List.push_back(new WorpPoint(x_, y_, LG->ReturnGraph(TypeMapChip, WORP_P)));
				Effect_List.push_back(new WorpEffect(x_, y_, LG->ReturnGraph(TypeEffect, Worp_P_Effect)));
			}else
			if (Map[y][x] == MCTYPE_KEY_1){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				MapObj_List.push_back(new Key_1(x_, y_, LG->ReturnGraph(TypeMapChip, KEY_1)));
			}else
			if (Map[y][x] == MCTYPE_DOOR_1){
				int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*x);
				int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*y);
				MapObj_List.push_back(new Door(x_, y_, LG->ReturnGraph(TypeMapChip, DOOR_1)));
			}
		}
	}
	return 0;
}


void StageScean::Draw(){

	CameraUpdate();

	for (int i = -1; i < MapYNum; i++){
		for (int n = -1; n < MapXNum; n++){
/*			int x_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*n) - CameraX;
			int y_ = (int)(MAPOBJECTSIZE / 2 + MAPOBJECTSIZE*i) - CameraY;

			if (RetrunSceanType() == Type_Stage2_1 || RetrunSceanType() == Type_Stage2_2)
				DrawRotaGraph(x_, y_, Magnification, 0.0f, LG->ReturnGraph(TypeMapChip, DESERTCHIP), TRUE);
			else
				DrawRotaGraph(x_, y_, Magnification, 0.0f, LG->ReturnGraph(TypeMapChip, GRASS), TRUE);*/
			int x_ = (int)(96 / 2 + 96*n) - CameraX;
			int y_ = (int)(96 / 2 + 96*i) - CameraY; 

			if (RetrunSceanType() == Type_Stage2_1 || RetrunSceanType() == Type_Stage2_2)
				DrawRotaGraph(x_, y_, 1.0f, 0.0f, LG->ReturnGraph(TypeMapChip, DESERTCHIP), TRUE);
			else
				DrawRotaGraph(x_, y_, 1.0f, 0.0f, LG->ReturnGraph(TypeMapChip, GRASS), TRUE);
		}
	}
	for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end(); itr++){
		(*itr)->Draw(CameraX,CameraY);
	}
	for (std::vector<Enemy*>::iterator itr = Enemy_List.begin(); itr != Enemy_List.end(); itr++){
		(*itr)->Draw(CameraX, CameraY);
	}
	for (std::vector<Effect*>::iterator itr = Effect_List.begin(); itr != Effect_List.end(); itr++){
		(*itr)->Draw(CameraX ,CameraY);
	}
	for (std::vector<Player*>::iterator itr = Player_List.begin(); itr != Player_List.end(); itr++){
		(*itr)->Draw(CameraX, CameraY);
	}
	for (std::vector<Bullet*>::iterator itr = Bullet_List.begin(); itr != Bullet_List.end(); itr++){
		(*itr)->Draw(CameraX, CameraY);
	}
}



void StageScean::UpdateList(){
	for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end(); itr++){
		(*itr)->update();
	}
	for (std::vector<Effect*>::iterator itr = Effect_List.begin(); itr != Effect_List.end(); itr++){
		(*itr)->update();
	}
	for (std::vector<Player*>::iterator itr = Player_List.begin(); itr != Player_List.end(); itr++){
		(*itr)->update();
	}
	for (std::vector<Enemy*>::iterator itr = Enemy_List.begin(); itr != Enemy_List.end(); itr++){
		(*itr)->update();
	}
	for (std::vector<Bullet*>::iterator itr = Bullet_List.begin(); itr != Bullet_List.end(); itr++){
		(*itr)->update();
	}
	update();
}


void StageScean::update(){
	Draw();
	Debug();

	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetNowState(State_Update);
			MMngr->PushMusic(STAGE_1_BGM,TRUE);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		if (PM->ReturnPlayerGameOverFlag() == true)SetNowState(State_GameOver);
		HitDicision();
		PM->update();
		StageEvent();
		EnemyAttckPart();
		PlayerAttackPart();
		MMngr->UpdateMusic();
		DeleteObj();
	}else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}else 
	if (ReturnNowState() == State_GameOver){
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
}



void StageScean::EnemyAttckPart(){
	for (std::vector<Enemy*>::iterator itr = Enemy_List.begin(); itr != Enemy_List.end(); itr++){
		if ((*itr)->ReturnAttackFlag() == TRUE){
			if ((*itr)->getEnemyType() == EnemyType_RABBIT)	Bullet_List.push_back(new RabbitBullet((*itr)->x, (*itr)->y,  5, Cal.ReturnRad((*itr)->x, (*itr)->y, PM->x, PM->y)));
			else
			if ((*itr)->getEnemyType() == EnemyType_BAT)	Bullet_List.push_back(new BatBullet((*itr)->x, (*itr)->y, 5, Cal.ReturnRad((*itr)->x, (*itr)->y, PM->x, PM->y)));
			else
			Bullet_List.push_back( new straightBullet( (*itr)->x, (*itr)->y, LG->ReturnGraph(TypeBullet, RICE_B), 5, Cal.ReturnRad((*itr)->x,(*itr)->y,PM->x,PM->y) ) );
		};
	}
}

void StageScean::PlayerAttackPart(){
	if (PM->ReturnShotFlag() == TRUE){
		Bullet_List.push_back(new Player1Bullet(PM->x, PM->y, LG->ReturnGraph(TypeBullet, 1), 5, PM->rad));
		MMngr->PushEffectSound(PLAYER1_ATTACKSOUND);
		return;
	}
	return;
}

void StageScean::HitDicision(){
	HittoMapObjandMover();
	HittoMapObjandBullet();
}

int StageScean::HittoMapObjandBullet(){
	for (std::vector<Bullet*>::iterator Bitr = Bullet_List.begin(); Bitr != Bullet_List.end(); Bitr++){
		for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end(); itr++){
			if (Cal.HittoBoxandCircle((*itr)->x, (*itr)->y, (*itr)->sizex, (*itr)->sizey, (*Bitr)->x, (*Bitr)->y, (*Bitr)->sizex ) == 1){
				(*Bitr)->SetFlag(FALSE);
				if ((*Bitr)->ReturnBulletType() == ENEMY_BULLET){ Effect_List.push_back(new HitBlockEffect((*Bitr)->x, (*Bitr)->y, Effect_Color_BLUE)); }
				else { Effect_List.push_back(new HitBlockEffect((*Bitr)->x, (*Bitr)->y, Effect_Color_WHITE)); }
			}
		}
		/*弾対敵*/
		if ((*Bitr)->ReturnBulletType() == PLAYER_BULLET){
			for (std::vector<Enemy*>::iterator itr = Enemy_List.begin(); itr != Enemy_List.end(); itr++){
				if (Cal.HittoCircleandCircle((*Bitr)->x, (*Bitr)->y, (*Bitr)->sizex, (*itr)->x, (*itr)->y, (*itr)->sizex/2)){
					(*Bitr)->SetFlag(FALSE);
					(*itr)->SetHitFlag(TRUE ,1);
				}
			}
		}else
		/*弾対プレイヤー*/
		if ((*Bitr)->ReturnBulletType() == ENEMY_BULLET){
			if (Cal.HittoCircleandCircle((*Bitr)->x, (*Bitr)->y, (*Bitr)->sizex, PM->x, PM->y, 10)){
				(*Bitr)->SetFlag(FALSE);
				PM->SetHitFlag(1);
			}
		}
	}

	return 0;
}


int StageScean::HittoMapObjandMover(){
	int x_1[4], x_2[4];//示す角度は0→1→2→3→0の順番
	int y_1[4], y_2[4];
	//ブロックとプレイヤー
	for (std::vector<Player*>::iterator Pitr = Player_List.begin(); Pitr != Player_List.end(); Pitr++){
		int onetime_x = PM->x - (*Pitr)->vectorx;
		int onetime_y = PM->y - (*Pitr)->vectory;
		x_2[0] = PM->x - (*Pitr)->sizex / 2;
		x_2[2] = PM->x + (*Pitr)->sizex / 2;
		y_2[0] = PM->y - (*Pitr)->sizey / 2;
		y_2[2] = PM->y + (*Pitr)->sizey / 2;
		for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end(); itr++){
			if ((*itr)->ReturnCollisionDetectionFlag() == TRUE){
				x_1[0] = (*itr)->x - (*itr)->sizex / 2;
				x_1[2] = (*itr)->x + (*itr)->sizex / 2;
				y_1[0] = (*itr)->y - (*itr)->sizey / 2;
				y_1[2] = (*itr)->y + (*itr)->sizey / 2;
				if (x_2[0] < x_1[2] && x_1[0] < x_2[2] && y_1[0] < y_2[2] && y_2[0] < y_1[2]){
					int __x = 100, __y = 100, Res_x = 10000, Res_y = 10000;

					if ((*itr)->getObjType() == ObjType_Map_Door && PM->getKeyNum() > 0){/*鍵付きドアの場合*/
						(*itr)->SetFlag(FALSE);
						PM->deleteItem(ObjType_Item_Key, 1);
					}

					if ((*Pitr)->vectorx != 0 && (*Pitr)->vectory != 0){//斜めの判定
						for (int i = 0; i < 4; i++){
							//if (CheckCrossVector(x_1[i], y_1[i], x_1[(i + 1) % 4], y_1[(i + 1) % 4], x_2[n], y_2[n], x_2[(n + 1) % 4], y_2[(n + 1) % 4], __x, __y) == 0)
							if (CheckCrossVector(x_1[i], y_1[i], x_1[(i + 1) % 4], y_1[(i + 1) % 4], PM->x - (*Pitr)->vectorx, PM->y - (*Pitr)->vectory, PM->x, PM->y, __x, __y) != 1){
								//return 0;
							}
							else
							if ((__x - onetime_x)*(__x - onetime_x) + (__y - onetime_y)*(__y - onetime_y) <= (Res_x - onetime_x)*(Res_x - onetime_x) + (Res_y - onetime_y)*(Res_y - onetime_y)){
								Res_x = __x;
								Res_y = __y;
								if (i == 0){
									PM->y = __y - (*Pitr)->sizey / 2;
									//PM->x = __x ;
								}
								else
								if (i == 1){
									//PM->y = __y;
									PM->x = __x + (*Pitr)->sizex / 2;
								}
								else
								if (i == 2){
									PM->y = __y + (*Pitr)->sizey / 2;
									//PM->x = __x ;
								}
								else
								if (i == 3){
									//PM->y = __y;
									PM->x = __x - (*Pitr)->sizex / 2;
								}
							}
						}
					}
					else{//縦方向or横方向どちらかの場合
						if ((*Pitr)->vectorx > 0){
							PM->x = (*itr)->x - (*itr)->sizex / 2 - (*Pitr)->sizex / 2;
						}
						else
						if ((*Pitr)->vectorx < 0){
							PM->x = (*itr)->x + (*itr)->sizex / 2 + (*Pitr)->sizex / 2;
						}

						if ((*Pitr)->vectory > 0){
							PM->y = (*itr)->y - (*itr)->sizey / 2 - (*Pitr)->sizey / 2;
						}
						else
						if ((*Pitr)->vectory < 0){
							PM->y = (*itr)->y + (*itr)->sizey / 2 + (*Pitr)->sizey / 2;
						}
					}
				}
			}
		}
		break;
	}
	
	//Blockと敵
	for (std::vector<Enemy*>::iterator Eitr = Enemy_List.begin(); Eitr != Enemy_List.end(); Eitr++){
		x_2[0] = (*Eitr)->x - (*Eitr)->sizex / 2;
		x_2[2] = (*Eitr)->x + (*Eitr)->sizex / 2;
		y_2[0] = (*Eitr)->y - (*Eitr)->sizey / 2;
		y_2[2] = (*Eitr)->y + (*Eitr)->sizey / 2;
		for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end(); itr++){
			x_1[0] = (*itr)->x - (*itr)->sizex / 2;
			x_1[2] = (*itr)->x + (*itr)->sizex / 2;
			y_1[0] = (*itr)->y - (*itr)->sizey / 2;
			y_1[2] = (*itr)->y + (*itr)->sizey / 2;
			if (x_2[0] < x_1[2] && x_1[0] < x_2[2] && y_1[0] < y_2[2] && y_2[0] < y_1[2]){
				if ((*Eitr)->vectorx > 0){
					(*Eitr)->x = (*itr)->x - (*itr)->sizex / 2 - (*Eitr)->sizex / 2;
				}
				else
				if ((*Eitr)->vectorx < 0){
					(*Eitr)->x = (*itr)->x + (*itr)->sizex / 2 + (*Eitr)->sizex / 2;
				}

				if ((*Eitr)->vectory > 0){
					(*Eitr)->y = (*itr)->y - (*itr)->sizey / 2 - (*Eitr)->sizey / 2;
				}
				else
				if ((*Eitr)->vectory < 0){
					(*Eitr)->y = (*itr)->y + (*itr)->sizey / 2 + (*Eitr)->sizey / 2;
				}
				break;
			}
		}
	}

	/*ItemとPlayer*/
	for (std::vector<Player*>::iterator Pitr = Player_List.begin(); Pitr != Player_List.end(); Pitr++){
		x_2[0] = PM->x - (*Pitr)->sizex / 2;
		x_2[2] = PM->x + (*Pitr)->sizex / 2;
		y_2[0] = PM->y - (*Pitr)->sizey / 2;
		y_2[2] = PM->y + (*Pitr)->sizey / 2;
		for (std::vector<MapObj*>::iterator itr = MapObj_List.begin(); itr != MapObj_List.end(); itr++){
			if ((*itr)->getObjType() == ObjType_Item_Key){
				x_1[0] = (*itr)->x - (*itr)->sizex / 2;
				x_1[2] = (*itr)->x + (*itr)->sizex / 2;
				y_1[0] = (*itr)->y - (*itr)->sizey / 2;
				y_1[2] = (*itr)->y + (*itr)->sizey / 2;
				if (x_2[0] < x_1[2] && x_1[0] < x_2[2] && y_1[0] < y_2[2] && y_2[0] < y_1[2]){
					PM->setItemList(ObjType_Item_Key);
					(*itr)->SetFlag(FALSE);
					break;
				}
			}
		}
	}
	


	return 0;
}

void StageScean::getPlayerItem(){
	
}




double ReturnVectDistanc(double x1, double y1, double x2, double y2){//ベクトル間の距離を返す関数
	return (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1);
}

double ReturnInnerProduct(double x1, double y1, double x2, double y2){//ベクトルの内積を返す関数
	return (x2*x1)+(y2*y1);
}

double CreatVector(double x1, double y1, double x2, double y2, double &ax, double &ay){//ベクトルを返す関数
	ax = x2 - x1;
	ay = y2 - y1;
	return 1;
}

double ReturnUnitVector(double x1, double y1,  double &ax, double &ay){//単位ベクトル
	double length = sqrt((x1*x1) + (y1*y1));
	ax = (double)x1 / length;
	ay = (double)y1 / length;
	return 0;
}


//http://qiita.com/ykob/items/ab7f30c43a0ed52d16f2
//http://www.sousakuba.com/Programming/gs_two_lines_intersect.html
int CheckCrossVector(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int &returnx, int &returny){//return 0 計算できず、or交点なし

	if (ReturnVectDistanc(x1, y1, x2, y2) == 0 || ReturnVectDistanc(x3, y3, x4, y4) == 0)return 0;

	double ax = 0, ay = 0, bx = 0, by = 0;
	double uax, uay, ubx, uby;				//単位ベクトル

	CreatVector(x1, y1, x2, y2, ax, ay);
	CreatVector(x3, y3, x4, y4, bx, by);

	ReturnUnitVector(ax, ay, uax, uay);
	ReturnUnitVector(bx, by, ubx, uby);

	double work1 = ReturnInnerProduct(uax,uay,ubx,uby);
	double work2 = 1 - work1*work1;

	if (work2 == 0)return 0;

	double c13x, c13y;

	CreatVector(x1, y1, x3, y3, c13x, c13y);

	double d1 = (ReturnInnerProduct(c13x, c13y, uax, uay) - work1 * ReturnInnerProduct(c13x, c13y, ubx, uby)) / work2;
	double d2 = (work1 * ReturnInnerProduct(c13x, c13y, uax, uay) - ReturnInnerProduct(c13x, c13y, ubx, uby)) / work2;


	double result_x[2], result_y[2];

	result_x[0] = x1 + d1 * uax;
	result_y[0] = y1 + d1 * uay;
	result_x[1] = x3 + d2 * ubx;
	result_y[1] = y3 + d2 * uby;
	if (ReturnVectDistanc(result_x[0], result_y[0], result_x[1], result_y[1]) <= 0.00001) {
		returnx = (int)result_x[1];
		returny = (int)result_y[1];
		return 1;
	}
	return 0;
}