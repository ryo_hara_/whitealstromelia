#include "SpecialScean.h"
#include "Load.h"

#define BLOCKSIZE 96

void StageFlame::update(){
	draw();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		if (PM->ReturnPlayerGameOverFlag() == true){
			SetNowState(State_GameOver);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
		}
		if (PM->ReturnClearFlag() == true){
			SetNowState(State_NextSceanAhead);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
		}
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
	else
	if (ReturnNowState() == State_GameOver){
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
}

void StageFlame::draw(){
	//枠(ブロック)の表示
	int x = 0 , y = 0; 
	double Rate = 1.0f;
	for (int i = 0; i < 7; i++){
		y = BLOCKSIZE / 2 + BLOCKSIZE * i;
		if (i < 5){
			for (int n = 0; n < 2; n++){
				x = BLOCKSIZE / 2 + BLOCKSIZE * n;
				DrawRotaGraph(x, y, Rate, 0.0f, LG->ReturnGraph(TypeMapChip, BLOCK), TRUE);
			}
		}else{
			for (int n = 0; n < 10; n++){
				x = BLOCKSIZE / 2 + BLOCKSIZE * n;
				DrawRotaGraph(x, y, Rate, 0.0f, LG->ReturnGraph(TypeMapChip, BLOCK), TRUE);
			}
		}
	}
	//フレーム上で立絵表示（試験的）
	SetDrawMode(DX_DRAWMODE_BILINEAR);
	DrawRotaGraph(100, 250, 0.5f, 0.0f, LG->ReturnGraph(TypeStandingGraph, BRAVE), TRUE);//224*120
	SetDrawMode(DX_DRAWMODE_NEAREST);

	//ライフ系表示
	DrawRotaGraph(112, 540   , 1.00f , 0.0f, LG->ReturnGraph(TypeUI, LIFEFLAME),TRUE);//224*120
	DrawRotaGraph( 56, 540-25, 1.30f , 0.0f, LG->ReturnGraph(TypeUI, LIFEHERT), TRUE);
	DrawRotaGraph(112, 540-25, 1.30f , 0.0f, LG->ReturnGraph(TypeUI, MULTIPLY_TYPE1), TRUE);
	DrawRotaGraph(168, 540-25, 1.30f , 0.0f, LG->ReturnGraph(TypeUI, NUMERAL_TYPE1 + PM->ReturnLife(0)), TRUE);

	DrawRotaGraph(56 , 540 + 25, 1.75f, 0.0f, LG->ReturnGraph(TypeMapChip, KEY_1), TRUE);
	DrawRotaGraph(112, 540 + 25, 1.30f, 0.0f, LG->ReturnGraph(TypeUI, MULTIPLY_TYPE1), TRUE);
	DrawRotaGraph(168, 540 + 25, 1.30f, 0.0f, LG->ReturnGraph(TypeUI, NUMERAL_TYPE1 + PM->getKeyNum()), TRUE);
}



void GameOver::draw(){
	DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeSceanGraph,GAMEOVER), TRUE);
	DrawRotaGraph(200, 500, 1.0f, 0.0f, LG->ReturnGraph(TypeSceanGraph, GAMEOVER_S1), TRUE);

};

void GameOver::update(){
	draw();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		if (IM->key[ATTACK] >= 1){
			SetNowState(State_NextSceanAhead);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
		}
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
	else
	if (ReturnNowState() == State_GameOver){
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
}

void Clear_Trial::draw(){
	DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeSceanGraph, GAMECLEAR), TRUE);
	DrawRotaGraph(250, 500, 1.0f, 0.0f, LG->ReturnGraph(TypeSceanGraph, GAMECLEAR_S1), TRUE);
	return;
}


void Clear_Trial::update(){
	draw();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		if (IM->key[ATTACK] >= 1){
			SetNowState(State_NextSceanAhead);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
		}
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
	else
	if (ReturnNowState() == State_GameOver){
		DC.update();
		if (DC.ReturnDarkChengeFlag() == false){
			SetFinishFlag(true);
		}
	}
	return;
}
