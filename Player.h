#include"Mover.h"
#include"Music.h"
#include <list>

#define MAPSTARTX 200 
#define MAPSTARTY 0


#define PLAYERTYPENUM 5

#define SHOTCOUNTDISTANCE 10

/*PlayerMngrの保持するアイテムリスト用クラス*/


class Item{
private:
	int type;
public:
	Item(int type){ setType(type); };
	void setType(int type){ this->type = type;}
	int	 getType(void){ return type; }
};


class PlayerMngr{
private:
	PlayerMngr();
	static PlayerMngr *playermngr;
	~PlayerMngr(){};
	int graph;
	int PlayerLife[PLAYERTYPENUM];
	int count;
	int HitFlag;
	int ShotFlag = FALSE;
	int ShotCount;
	bool Clearflag;
public:
	std::list<Item*> ItemList;
	LoadSound * LS = LoadSound::getInstance();
	MusicMngr * MMngr = MusicMngr::getInstance();
	void Draw(int _x, int _y){};
	void update();
	static PlayerMngr* getInstance(){ return playermngr; }
	int x, y;
	int alpha;
	double rad;

	void setItemList(int type);
	int deleteItem(int type, int num);

	int		getKeyNum(void);
	void	SetHitFlag			(int DamagePoint);
	void	SetShotFlag			(int flag);
	void	Initialize			(void);
	bool	ReturnPlayerGameOverFlag(void)	{ if (PlayerLife[0] < 0)return true; else return false; }
	int		ReturnLife			(int num)	{ return PlayerLife[num]; };
	void	InitializationLife	(void)		{ for (int i = 0; i < PLAYERTYPENUM; i++)PlayerLife[i] = 5; }
	void	SetClearFlag		(bool _flag){ this->Clearflag = _flag; };
	bool	ReturnClearFlag		(void)		{ return Clearflag; };
	int		ReturnShotFlag		(void)		{
		/*if (flag == TRUE){
		if (ShotCount == 0)ShotFlag = TRUE;
		else ShotFlag = FALSE;
		ShotCount = SHOTCOUNTDISTANCE;
		}*/
		if (ShotFlag == TRUE){

			SetShotFlag(FALSE);
			return TRUE;
		}
		return FALSE;
	}
};

class Player :public actor{
public:
	Player();
	PlayerMngr *PM = PlayerMngr::getInstance();
	InputMngr *IM = InputMngr::getInstance();
};

class Player1 : public Player{
public:
	void Draw(int _x , int _y);
	void update();
};
class Player2: public Player{
public:
	void Draw(int _x, int _y);
	void update();
};
class Player3 : public Player{
public:
	void Draw(int _x, int _y);
	void update();
};
