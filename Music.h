#ifndef _MUSIC_H_
#define _MUSIC_H_

#include "Load.h"

class MusicMngr{
private:

	int nowPlayMusic;
	int nextPlayMusic;
	int nowPlayMusicVol;
	int nextPlayMusicVol;
	int FeedinFlag;
	int MaxBGMVol;

public:

	LoadSound *LS = LoadSound::getInstance();

	MusicMngr(){
		this->MaxBGMVol			= 255;
		this->nowPlayMusic		= -1;
		this->nowPlayMusicVol	= MaxBGMVol;
		this->nextPlayMusic		= -1;
		this->nextPlayMusicVol	= 0;
		this->FeedinFlag		= 0;
	}
	~MusicMngr(){}
	static MusicMngr* getInstance(){
		static MusicMngr * musicmngr = new MusicMngr();
		return musicmngr;
	}

	void	UpdateMusic();
	int		PushMusic(int music_num , int feedinflag);
	int		PushEffectSound(int music_num);
	int		SetMaxBGMVolume(int Vol/*最小0,最大値255*/){ MaxBGMVol = Vol; return MaxBGMVol; };
	int		ReturnMaxBGMVolume(){ return MaxBGMVol; }
};




#endif


/*OwnCloud*/