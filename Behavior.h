#ifndef _BEHAVIOR_
#define _BEHAVIOR_

#include <stdlib.h>

class independentPattern{
private:
	int movePatternCounter;
public:
	independentPattern(){ movePatternCounter = rand() % 4; }
	void move(double &x, double &y , double &vector_x ,double &vector_y , int count , int ChangeInterval);
};


class SpeedyindependentPattern{
public:
	void move(double &x, double &y, double &vector_x, double &vector_y, int count, int ChangeInterval);
};




#endif