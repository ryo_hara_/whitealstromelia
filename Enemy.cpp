#include "Enemy.h"

#define MOVEDIST 3


void Slime::update(){
	IPattern.move(x,y,vectorx,vectory , count , 100);

	count++;

	if (count % 100 == 0)
		SetAttackFlag(TRUE);
	else
		SetAttackFlag(FALSE);

	if (vectorx > 0)
		graphAllocat = 3;
	else
	if (vectorx < 0)
		graphAllocat = 2;
	else
	if (vectory > 0)
		graphAllocat = 0;
	else
	if (vectory < 0)
		graphAllocat = 1;

	graph = LG->ReturnGraph(TypeEnemy,SLIME+graphAllocat);

	if (ReturnHitFlag() == TRUE){
		if (Hittimecounter == 0){//ダメージ添加
			SubHP(1);
			MMngr->PushEffectSound(ENEMY_HITSOUND_1);
		}
		if (Hittimecounter > 80){
			SetHitFlag(FALSE,0);
			Hittimecounter = 0;
		}
		else{
			Hittimecounter++;
		}
		if (Hittimecounter% 10 < 5)alpha = 255;
		else alpha = 125;

	}else{
		int Hittimecounter = 0;
		alpha = 255;
	}

	if (ReturnHP() <= 0)SetFlag(FALSE);

	return;
}



void Rabbit::update(){
	IPattern.move(x, y, vectorx, vectory, count, 100);

	count++;

	if (count % 100 == 0)
		SetAttackFlag(TRUE);
	else
		SetAttackFlag(FALSE);

	if (vectorx > 0)
		graphAllocat = 3;
	else
	if (vectorx < 0)
		graphAllocat = 2;
	else
	if (vectory > 0)
		graphAllocat = 0;
	else
	if (vectory < 0)
		graphAllocat = 1;

	graph = LG->ReturnGraph(TypeEnemy, RABBIT + graphAllocat);


	if (ReturnHitFlag() == TRUE){
		if (Hittimecounter == 0){//ダメージ添加
			MMngr->PushEffectSound(ENEMY_HITSOUND_1);
			SubHP(1);
		}
		if (Hittimecounter > 80){
			SetHitFlag(FALSE, 0);
			Hittimecounter = 0;
		}
		else{
			Hittimecounter++;
		}
		if (Hittimecounter % 10 < 5)alpha = 255;
		else alpha = 125;

	}else{
		int Hittimecounter = 0;
		alpha = 255;
	}
	if (ReturnHP() <= 0)SetFlag(FALSE);
}


void Bat::update(){
	IPattern.move(x, y, vectorx, vectory, count, 100);

	count++;

	if (count % 100 == 0)
		SetAttackFlag(TRUE);
	else
		SetAttackFlag(FALSE);

	if (vectorx > 0)
		graphAllocat = 3;
	else
	if (vectorx < 0)
		graphAllocat = 2;
	else
	if (vectory > 0)
		graphAllocat = 0;
	else
	if (vectory < 0)
		graphAllocat = 1;

	graph = LG->ReturnGraph(TypeEnemy, BAT + graphAllocat);


	if (ReturnHitFlag() == TRUE){
		if (Hittimecounter == 0){//ダメージ添加
			MMngr->PushEffectSound(ENEMY_HITSOUND_1);
			SubHP(1);
		}
		if (Hittimecounter > 80){
			SetHitFlag(FALSE, 0);
			Hittimecounter = 0;
		}
		else{
			Hittimecounter++;
		}
		if (Hittimecounter % 10 < 5)alpha = 255;
		else alpha = 125;

	}
	else{
		int Hittimecounter = 0;
		alpha = 255;
	}
	if (ReturnHP() <= 0)SetFlag(FALSE);

}

void Bird::update(){
	IPattern.move(x, y, vectorx, vectory, count, 100);

	count++;

	if (count % 100 == 0)
		SetAttackFlag(TRUE);
	else
		SetAttackFlag(FALSE);

	if (vectorx > 0)
		graphAllocat = 3;
	else
	if (vectorx < 0)
		graphAllocat = 2;
	else
	if (vectory > 0)
		graphAllocat = 0;
	else
	if (vectory < 0)
		graphAllocat = 1;

	graph = LG->ReturnGraph(TypeEnemy, BIRD + graphAllocat);

	if (ReturnHitFlag() == TRUE){
		if (Hittimecounter == 0){//ダメージ添加
			MMngr->PushEffectSound(ENEMY_HITSOUND_1);
			SubHP(1);
		}
		if (Hittimecounter > 80){
			SetHitFlag(FALSE, 0);
			Hittimecounter = 0;
		}
		else{
			Hittimecounter++;
		}
		if (Hittimecounter % 10 < 5)alpha = 255;
		else alpha = 125;

	}
	else{
		int Hittimecounter = 0;
		alpha = 255;
	}
	if (ReturnHP() <= 0)SetFlag(FALSE);

}
