#include "Talk.h"


void FirstTalk::update(){
	TalkSkip();
	talk();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		IM->SetInputFlag(true);
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetFinishFlag(true);
		}
	}

}

void FirstTalk::talk(){
	Draw();
	static int Char1Dim = 135;
	static int Char2Dim = 0;
	static int Char3Dim = 0;
	//背景
	SetDrawMode(DX_DRAWMODE_BILINEAR);
	if (ReturnTalkPoint() <= 23){DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, FOUNTAIN), TRUE);}
	else{DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, WITCHHOUSE), TRUE);}
	
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char1Dim);
	DrawRotaGraph(150, 600, 1.0f, 0.0f, LG->ReturnGraph(TypeStandingGraph, BRAVE), TRUE);//Char1
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char2Dim);
	DrawRotaGraph(650, 600, 0.95f, 0.0f, LG->ReturnGraph(TypeStandingGraph, ARCHER), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char3Dim);
	DrawRotaGraph(400,600, 0.90f, 0.0f, LG->ReturnGraph(TypeStandingGraph, WITCH), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);

	SetDrawMode(DX_DRAWMODE_BILINEAR);//線形補間関数
	//テキストボックス
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 200);
	DrawBox(50, 450, 750, 575, GetColor(0, 0, 0), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);


	if (ReturnTalkPoint() == 54){
		SetNowState(State_NextSceanAhead);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
	}
		


		/*　序章　−旅立ち−*/

	switch (ReturnTalkPoint()){

	case -6:
		Char1Dim = 135;
		Char2Dim = 0;
		Char3Dim = 0;
		SetFontSize(24);
		DrawString(70, 470, "　ここは剣と魔法の存在している世界。とはいいながらも", GetColor(235, 235, 235));
		DrawString(70, 500, "争いなどはなく（ご都合主義的に）人々は平和な日々を穏", GetColor(235, 235, 235));
		DrawString(70, 530, "やかに過ごしていた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case -5:
		DrawString(70, 470, "だがそんな日々は唐突に終わりを告げることになる。突如", GetColor(235, 235, 235));
		DrawString(70, 500, "現れた異形の生命体「魔物」そして、それを率いる魔王と", GetColor(235, 235, 235));
		DrawString(70, 530, "呼ばれる存在。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case -4:
		DrawString(70, 470, "魔王は人類に宣戦布告し、人類と魔物との血で血を洗う戦争", GetColor(235, 235, 235));
		DrawString(70, 500, "が始まったのだ。しかし戦況は明らかであった。", GetColor(235, 235, 235));
		DrawString(70, 530, "", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case -3:
		DrawString(70, 470, "それまで戦争はおろか争い事すら碌に経験のなかった人々は", GetColor(235, 235, 235));
		DrawString(70, 500, "魔物に一方的に蹂躙されるほかなく、人類は絶体絶命の危機", GetColor(235, 235, 235));
		DrawString(70, 530, "に瀕していた・・・・・・。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case -2:
		DrawString(70, 470, "何てことはなく、魔王と魔物たちはとても友好的。", GetColor(235, 235, 235));
		DrawString(70, 500, "争いなんて持ってのほか。人類と魔物は互いに良き", GetColor(235, 235, 235));
		DrawString(70, 530, "隣人として生活していた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case -1:
		DrawString(70, 470, "これは、そんな世界で起きた意外と大きかったりするかも", GetColor(235, 235, 235));
		DrawString(70, 500, "しれない愛の物語である。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 0:
		Char1Dim = 135;
		Char2Dim = 0;
		Char3Dim = 0;
		SetFontSize(32);
		DrawString(70, 510, "〜王都　噴水前〜", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 1:
		SetFontSize(24);
		DrawString(70, 470, "いつも通りの朝、鶏が鳴き、朝日が昇り始めると同時に王都", GetColor(235, 235, 235));
		DrawString(70, 500, "の朝は始まる。ぽつぽつと人が見え始め、俄かに活気づいて", GetColor(235, 235, 235));
		DrawString(70, 530, "きたころ。都の中心に存在する噴水から物語は始まる。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 2:
		DrawString(70, 470, "「何で、誰も来ないんだよ・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 3:
		DrawString(70, 470, "・・・・・訂正しよう始まるどころか頓挫しかかっていた。",GetColor(235, 235, 235));
		DrawString(70, 500, "噴水前に一人寂しく立っているのはこの物語の主人公ラズ。", GetColor(235, 235, 235));
		DrawString(70, 530, "周りからは勇者と呼ばれている。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 4:
		DrawString(70, 470, "何故勇者なのかは追々語る機会があるだろう。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 5:
		DrawString(70, 470, "「確かに朝早いけどさぁ、普通集まるでしょ。", GetColor(235, 235, 235));
		DrawString(70, 500, "  旅立ちの日に遅刻って控えめに見てもおかしいでしょ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 6:
		DrawString(70, 470, "　そんな彼に近づく影が一つ。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 7:
		DrawString(70, 470, "「すまない、待たせたか？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 8:
		DrawString(70, 470, "「ううん、今来たとこ・・・・・・。", GetColor(235, 235, 235));
		DrawString(70, 500, "  じゃねーよッ！！何この恋人みたいなやり取り！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 9:
		DrawString(70, 470, "「急に叫んでどうした？近所迷惑だぞ勇者。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 10:
		DrawString(70, 470, "「お前のせいだろうが！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 11:
		DrawString(70, 470, "「少し落ち着いたらどうだ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 12:
		DrawString(70, 470, "「お前がそれを言うのか弓使い・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 13:
		DrawString(70, 470, "出てきて早々恋人のようなやり取りをした彼は弓使いの", GetColor(235, 235, 235));
		DrawString(70, 500, "アゲート。勇者の仲間の一人である。本来ならもう一人", GetColor(235, 235, 235));
		DrawString(70, 530, "仲間がいるはずなのだが。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 14:
		DrawString(70, 470, "「それはそうとして、もう一人はどうした？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 15:
		DrawString(70, 470, "「お前が言うなよ・・・・・・。", GetColor(235, 235, 235));
		DrawString(70, 500, "  あいつはまだ来てないよ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 16:
		DrawString(70, 470, "「こんな事で大丈夫なのか？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 17:
		DrawString(70, 470, "「だからお前が言うなっての。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 18:
		DrawString(70, 470, "「大事な旅立ちの日に遅刻するとは。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 19:
		DrawString(70, 470, "「・・・・・・（#^ω^）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 20:
		DrawString(70, 470, "「大方寝坊でもしているのだろうが。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 21:
		DrawString(70, 470, "「・・・・・・だろうな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 22:
		DrawString(70, 470, "「仕方ない、あいつの家に行くとするか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 23:
		DrawString(70, 470, "「そーですね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

		/*******************************************************/

	case 24:
		SetFontSize(32);
		DrawString(70, 510, "―魔法使い宅―", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 25:
		SetFontSize(24);
		DrawString(70, 470, "いつも悩むんだけど、こういう時のノックは何回すればい", GetColor(235, 235, 235));
		DrawString(70, 500, "いんだ？", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 26:
		DrawString(70, 470, "「確か３回らしいぞ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 27:
		DrawString(70, 510, "「へー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 28:
		SetFontSize(32);
		DrawString(70, 510, "コンコンコンッ！", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 29:
		DrawString(70, 510, "・・・・・・", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 30:
		SetFontSize(24);
		DrawString(70, 470, "「出ないな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 31:
		DrawString(70, 470, "「もう少し強めにしたどうだ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 32:
		SetFontSize(32);
		DrawString(70, 510, "ゴンゴンゴンッ！！", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 33:
		DrawString(70, 510, "・・・・・・ガチャッ", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 34:
		SetFontSize(24);
		DrawString(70, 470, "「人が気持ちよく寝てるのに何さ〜。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 35:
		DrawString(70, 470, "如何にも寝起きといった風で扉から出てきたのは", GetColor(235, 235, 235));
		DrawString(70, 500, "魔法使いであるローズ。怠惰なところが玉に瑕", GetColor(235, 235, 235));
		DrawString(70, 530, "だが魔法の腕はピカイチな人物である。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 36:
		DrawString(70, 470, "「何さ〜、じゃねえよっ！！今日旅立つって言ったろ！？", GetColor(235, 235, 235));
		DrawString(70, 500, "　噴水前に集合だって。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 37:
		DrawString(70, 470, "「ん〜、そういえばそうだったねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 38:
		DrawString(70, 470, "「ほら、早く準備して出発するぞ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 39:
		DrawString(70, 470, "「何か面倒だし、旅に出るのやめない〜。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 40:
		DrawString(70, 470, "「やめないから！！さっさと準備してきなさい！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 41:
		SetFontSize(32);
		DrawString(70, 510, "数分後", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 42:
		SetFontSize(24);
		DrawString(70, 470, "「準備終わったよ〜。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 43:
		DrawString(70, 470, "「よし、じゃあ出発するか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 44:
		DrawString(70, 470, "「目的地はどこだ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 45:
		DrawString(70, 470, "「魔王の城だよ！！何で忘れてるのさ！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 46:
		DrawString(70, 470, "「そうだったな」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 47:
		DrawString(70, 470, "「どうやって行くの〜？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 48:
		DrawString(70, 470, "「まずは草原を抜けて砂漠を目指す。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 49:
		DrawString(70, 470, "「へー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 50:
		DrawString(70, 470, "「準備は大丈夫だな？。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 51:
		DrawString(70, 470, "「「おお（うん）」」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2_3);
		break;
	case 52:
		DrawString(70, 470, "「じゃあ、改めて魔王の城目指してしゅっぱーつ！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 53:
		DrawString(70, 470, "「「おー！！」」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2_3);
		break;
	}


	if (IM->key[ATTACK] % 10 == 1)AddTalkPoint();


	switch (ReturnTalkChar()){
	case Talk_Null:
		Char1Dim = 135;
		if(Char2Dim != 0)Char2Dim = 135;
		if(Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char1:
		Char1Dim = 255;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char2:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char3:
		if (Char1Dim != 0)Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		Char3Dim = 255;
		break;
	case Talk_Char2_3:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		Char3Dim = 255;
		break;
	}


}

void FirstTalk::Draw(){
}