#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "Mover.h"
#include "Behavior.h"
#include <math.h>

enum EnemyType{
	EnemyType_SLIME,
	EnemyType_RABBIT,
	EnemyType_BIRD,
	EnemyType_BAT,
	EnemyType_GHOST,
	EnemyType_GOLEM,
	EnemyType_DRAGON
};

class Slime : public Enemy{
public:

	Slime(int _x, int _y, int grphandle){
		this->sizex	= MAPOBJECTSIZE;
		this->sizey	= MAPOBJECTSIZE;
		this->x		= _x;
		this->y		= _y;
		this->graph	= grphandle;
		this->count	= rand() % 100;
		setEnemyType(EnemyType_SLIME);
	};
	~Slime(){};

	independentPattern IPattern;					/*�ړ��p�N���X�̈Ϗ�*/

	void Draw(int _x, int _y){
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);/*���l��MAX�͂Q�T�T*/
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph , TRUE);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	};
	void update();
};


class Rabbit : public Enemy{
public:
	Rabbit(int _x, int _y, int grphandle){
		this->sizex	= MAPOBJECTSIZE;
		this->sizey	= MAPOBJECTSIZE;
		this->x		= _x;
		this->y		= _y;
		this->graph	= grphandle;
		this->count	= rand() % 100;
		setEnemyType(EnemyType_RABBIT);
	};
	~Rabbit(){};

	independentPattern IPattern;					/*�ړ��p�N���X�̈Ϗ�*/


	void Draw(int _x, int _y){
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);/*���l��MAX�͂Q�T�T*/
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	};
	void update();
};



class Bird : public Enemy{
public:
	Bird(int _x, int _y, int grphandle){
		this->sizex = MAPOBJECTSIZE;
		this->sizey = MAPOBJECTSIZE;
		this->x		= _x;
		this->y		= _y;
		this->graph = grphandle;
		this->count = rand() % 100;
		setEnemyType(EnemyType_BIRD);
	};
	~Bird(){};
	independentPattern IPattern;

	void Draw(int _x, int _y){
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);/*���l��MAX�͂Q�T�T*/
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	};
	void update();
};

class Bat : public Enemy{
public:
	Bat(int _x, int _y, int grphandle){
		sizex = MAPOBJECTSIZE;
		sizey = MAPOBJECTSIZE;
		x = _x;
		y = _y;
		graph = grphandle;
		this->count = rand() % 100;
		setEnemyType(EnemyType_BAT);
	};
	~Bat(){};
	independentPattern IPattern;

	void Draw(int _x, int _y){
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);/*���l��MAX�͂Q�T�T*/
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	};
	void update();
};

class Ghost : public Enemy{
public:
	Ghost(int _x, int _y, int grphandle){
		sizex = MAPOBJECTSIZE;
		sizey = MAPOBJECTSIZE;
		x = _x;
		y = _y;
		graph = grphandle;
		this->count = rand() % 100;
		setEnemyType(EnemyType_GHOST);
	};
	~Ghost(){};

	void Draw(int _x, int _y){
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
	};
	void update(){};
};


class Golem : public Enemy{
public:
	Golem(int _x, int _y, int grphandle){
		sizex = MAPOBJECTSIZE;
		sizey = MAPOBJECTSIZE;
		x = _x;
		y = _y;
		graph = grphandle;
		this->count = rand() % 100;
		setEnemyType(EnemyType_GOLEM);
	};
	~Golem(){};

	void Draw(int _x, int _y){
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
	};
	void update(){};
};

class Dragon : public Enemy{
public:
	Dragon(int _x, int _y, int grphandle){
		sizex = MAPOBJECTSIZE;
		sizey = MAPOBJECTSIZE;
		x = _x;
		y = _y;
		graph = grphandle;
		this->count = rand() % 100;
		setEnemyType(EnemyType_DRAGON);
	};
	~Dragon(){};

	void Draw(int _x, int _y){
		DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
	};
	void update(){};
};

#endif