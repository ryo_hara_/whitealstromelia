#include "Player.h"
#include "Calculate.h"
PlayerMngr *PlayerMngr::playermngr = new PlayerMngr();




PlayerMngr::PlayerMngr(){
	Clearflag = false;
	for (int i = 0; i < PLAYERTYPENUM; i++)PlayerLife[i] = 5;
	x = 24;//240;
	y = 24;//16;
	count = 0;
	HitFlag = FALSE;
	ShotFlag = FALSE;
	ShotCount = 0;
	alpha = 255;
	rad = 0;
}

void PlayerMngr::Initialize(){
	Clearflag = false;
	for (int i = 0; i < PLAYERTYPENUM; i++)PlayerLife[i] = 5;
	x = 24;//240;
	y = 24;//16;
	count = 0;
	HitFlag = FALSE;
	ShotFlag = FALSE;
	alpha = 255;
	rad = 0;

}

void PlayerMngr::update(){
	if (HitFlag == TRUE &&count == 0){
		PlayerLife[0]--;
	}
	if (HitFlag == TRUE){
		if (count > 80){
			HitFlag = FALSE; 
			count = 0;
		}else{
			count++;
		}
		if (count % 20 < 10)alpha = 255;
		else alpha = 125;
	}else{
		count = 0;
		alpha = 255;
	}

	if (ShotCount > 0){ ShotCount--; }


	return;
}

void PlayerMngr::SetHitFlag(int DamagePoint){
	if (DamagePoint > 0)HitFlag = TRUE;
	MMngr->PushEffectSound(HIT_SOUND_1);
}

void PlayerMngr::SetShotFlag(int flag){
	ShotFlag = flag;
	return;
}


int PlayerMngr::getKeyNum(){
	int keynum = 0;
	for (std::list<Item*>::iterator itr = ItemList.begin(); itr != ItemList.end();){
		if ((*itr)->getType() == ObjType_Item_Key) {
			keynum++;
		}
		itr++;
	}
	if (keynum > 9)keynum = 9;
	return keynum;
}

void PlayerMngr::setItemList(int type){
	ItemList.push_back(new Item(type));
	return;
}

//バグが起きる可能性があるのはアイテム消去時
int PlayerMngr::deleteItem(int type, int num){
	int deletenum = num;
	for (std::list<Item*>::iterator itr = ItemList.begin(); itr != ItemList.end();){
		if (deletenum <= 0)break;
		if ((*itr)->getType() == type) {
			delete (*itr);
			itr = ItemList.erase(itr);
			if (deletenum > 0)deletenum--;
			continue;
		}
		itr++;
	}
	return 0;
}



Player::Player(){
	sizex = 20 * MAGNIFICATION;
	sizey = 20 * MAGNIFICATION;
	collision_type = TYPE_RECTANGLE;
}



void Player1::Draw(int _x, int _y){
	SetDrawBlendMode(DX_BLENDMODE_ALPHA,PM->alpha);/*α値のMAXは２５５*/
	DrawRotaGraph(PM->x - _x, PM->y  - _y, 1.0, 0.0f, graph, TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

void Player1::update(){
	vectorx = 0;
	vectory = 0;

	if (IM->key[UP] > 0){
		PM->y -= 3;
		vectory = -3;
		PM->rad = 3*(PI / 2);
	}else
	if (IM->key[DOWN] > 0){
		PM->y += 3;
		vectory = 3;
		PM->rad = PI / 2;
	}else
	if (IM->key[RIGHT] > 0){
		PM->x += 3;
		vectorx = 3;
		PM->rad = 0.0f;
	}else
	if (IM->key[LEFT] > 0){
		PM->x -= 3;
		vectorx = -3;
		PM->rad = PI;
	}
	
	if (vectorx > 0)
		graphAllocat = 2;
	else
	if (vectorx < 0)
		graphAllocat = 1;
	else
	if (vectory > 0)
		graphAllocat = 0;
	else
	if (vectory < 0)
		graphAllocat = 3;


	if (IM->key[ATTACK] % 30 == 1)
		PM->SetShotFlag(TRUE);



	graph = LG->ReturnGraph(TypePlayerChara, HERO_1 + graphAllocat);
}
