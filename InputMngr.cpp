#include "DxLib.h"
#include "InputMngr.h"

InputMngr* InputMngr::inputmngr = new InputMngr();


InputMngr::InputMngr(){
	Inputflag = true;
	for (int i = 0; i < KEYTYPENUM; i++)key[i] = 0;
	keypost[UP] = KEY_INPUT_UP;//PAD_INPUT_UP;
	keypost[DOWN] = KEY_INPUT_DOWN;//PAD_INPUT_DOWN;
	keypost[RIGHT] = KEY_INPUT_RIGHT;//PAD_INPUT_RIGHT;
	keypost[LEFT] = KEY_INPUT_LEFT;//PAD_INPUT_LEFT;
	keypost[ATTACK] = KEY_INPUT_Z;//PAD_INPUT_Z;
	keypost[CANSEL] = KEY_INPUT_X;//PAD_INPUT_X;
	keypost[ESCAPE] = KEY_INPUT_ESCAPE;
	keypost[DEBUG_P] = KEY_INPUT_P;
	keypost[TALKSKIP] = KEY_INPUT_SPACE;
}
InputMngr::~InputMngr(){}


int InputMngr::GetInput(){
	static int iKey[256] = { {0} };
	char NowStateKey[256]; // 現在のキーの入力状態を格納する
	GetHitKeyStateAll(NowStateKey); // 全てのキーの入力状態を得る

	if (Inputflag == false){
		for (int i = 0; i < 256; i++){ iKey[i] = 0;}
		for (int i = 0; i < KEYTYPENUM; i++){ key[i] = 0; }
		return 0;
	}

	for (int i = 0; i<256; i++){
		if (NowStateKey[i] != 0){ // i番のキーコードに対応するキーが押されていたら
			iKey[i]++;     // 加算
		}
		else {              // 押されていなければ
			iKey[i] = 0;   // 0にする
		}
	}
	for (int i = 0; i < KEYTYPENUM; i++){
		key[i] = iKey[keypost[i]];
	}
	return 0;
}