#ifndef _EFFECT_H_
#define _EFFECT_H_

#include "Mover.h"
#include "Game.h"

class WorpEffect : public Effect{

public:
	Game *GameU = Game::getInstance();
	WorpEffect(int _x, int _y, int graphHandle);
	~WorpEffect(){};

	int	GraphNum;

	void update();
	void Draw(int _x , int _y);
};


class ExploadEffect : public Effect{

public:
	Game *GameU = Game::getInstance();
	ExploadEffect(int _x, int _y);
	void update();
	void Draw(int _x, int _y);
};


class HitBlockEffect : public Effect{
private:
	int color;
public:
	Game *GameU = Game::getInstance();
	HitBlockEffect(int _x, int _y, int color);
	void update();
	void Draw(int _x, int _y);
};

enum EffectColorType{
	Effect_Color_BLUE,
	Effect_Color_WHITE
};

#endif