#ifndef _BULLET_H_
#define _BULLET_H_
#include <math.h>
#include "Mover.h"

class straightBullet : public Bullet {
private:
public:
	straightBullet(int _x, int _y, int grphandle ,int speed , double _rad){
		this->x			= _x;
		this->y			= _y;
		this->graph		= grphandle;
		this->rad = Cal.returnQuarterNearRad(_rad);
		this->vectorx	= speed*cos(this->rad);
		this->vectory	= speed*sin(this->rad);
		//this->rad		= _rad;
		this->sizex		= MAGNIFICATION * 10;
		this->sizey		= MAGNIFICATION * 6;
		this->SetBulletType(ENEMY_BULLET);
	}
	void update	();
	void Draw	(int _x , int _y);
};


class Player1Bullet : public Bullet{
private:
public:
	Player1Bullet(double _x , double _y , int graphhandle , int speed , double _rad){
		this->x			= _x;
		this->y			= _y;
		this->graph		= graphhandle;
		this->vectorx	= speed*cos(_rad);
		this->vectory	= speed*sin(_rad);
		this->rad		= _rad;
		this->sizex		= 1.0f * 16;
		this->sizey		= 1.0f * 6;
		this->SetBulletType(PLAYER_BULLET);
	}
	void update	();
	void Draw	(int _x , int _y);
};


class RabbitBullet : public Bullet{
private:

public:
	RabbitBullet(double _x, double _y , int speed, double _rad){
		this->x = _x;
		this->y = _y;
		this->graph = LG->ReturnGraph(TypeBullet,RABBIT_BULLET);
		this->vectorx = speed*cos(_rad);
		this->vectory = speed*sin(_rad);
		this->rad = _rad;
		this->sizex = 1.0f * 16;
		this->sizey = 1.0f * 6;
		this->SetBulletType(ENEMY_BULLET);
	}
	void update();
	void Draw(int _x, int _y);
};

class BirdBullet : public Bullet{
private:

public:
	BirdBullet(double _x, double _y, int speed, double _rad){
		this->x = _x;
		this->y = _y;
		this->graph = LG->ReturnGraph(TypeBullet, BIRD_BULLET);
		this->vectorx = speed*cos(_rad);
		this->vectory = speed*sin(_rad);
		this->rad = _rad;
		this->sizex = 1.0f * 16;
		this->sizey = 1.0f * 6;
		this->SetBulletType(ENEMY_BULLET);
	}
	void update();
	void Draw(int _x, int _y);
};

class BatBullet : public Bullet{
private:

public:
	BatBullet(double _x, double _y, int speed, double _rad){
		this->x = _x;
		this->y = _y;
		this->graph = LG->ReturnGraph(TypeBullet, BAT_BULLET);
		this->vectorx = speed*cos(_rad);
		this->vectory = speed*sin(_rad);
		this->rad = _rad;
		this->sizex = 1.0f * 16;
		this->sizey = 1.0f * 6;
		this->SetBulletType(ENEMY_BULLET);
	}
	void update();
	void Draw(int _x, int _y);
};



#endif