#ifndef _DARKCHANGE_H_
#define _DARKCHANGE_H_
#include "DxLib.h"



enum DarkChengeType{
	DARKCHENGE_Type_Null = 0,
	DARKCHENGE_Type_Feedin = 1, //徐々に明るくなる
	DARKCHENGE_Type_Feedout	    //徐々に暗くなる
};


class DarkChange {//シーン切替時の暗転用の委譲元クラス
private:
	int Dim;
	int flag;
public:
	DarkChange(){
		this->Dim = 255;
		this->flag = 0;
	};
	int ReturnDarkChengeFlag(){ return this->flag; }
	void SetDarkChenge(int flag){
		if (flag == DARKCHENGE_Type_Feedin){
			this->Dim = 255;
			this->flag = DARKCHENGE_Type_Feedin;
		}else
		if (flag == DARKCHENGE_Type_Feedout){
			this->Dim = 0;
			this->flag = DARKCHENGE_Type_Feedout;
		}
	};
	void update(){//描画、更新を内包した関数、形式上この関数は関数の一番後ろに置くべき
		if (this->flag == DARKCHENGE_Type_Feedin){
			this->Dim -= 5;
			if (this->Dim <= 0){ 
				this->flag = DARKCHENGE_Type_Null; this->Dim = 0; }
		}else
		if (this->flag == DARKCHENGE_Type_Feedout){
			this->Dim += 5;
			if (this->Dim >= 255){ this->flag = DARKCHENGE_Type_Null; Dim = 255; }
		}
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, this->Dim);
		DrawBox(-50, -50, 850, 650, GetColor(0, 0, 0), TRUE);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		return;
	}
};


#endif