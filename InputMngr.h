#ifndef _INPUTMNGR_H_
#define _INPUTMNGR_H_

#define KEYTYPENUM 7+1+1

/*デバッグスイッチの添加*/

class InputMngr{
private:
	InputMngr();
	static InputMngr* inputmngr;
	~InputMngr();
	bool Inputflag;
public:
	static InputMngr* getInstance(){ return inputmngr; }
	int		key[KEYTYPENUM];	//各キーの入力フレームを格納
	int		keypost[KEYTYPENUM];//各キーの割当てを格納
	int		GetInput(void);
	void	SetInputFlag(bool i){ Inputflag = i; };
	bool	ReturnInputFlag(void){ return Inputflag; };

};



enum keytype{ UP, DOWN, RIGHT, LEFT, ATTACK, CANSEL, ESCAPE ,DEBUG_P,TALKSKIP};
enum playerinput{ CUT, CONNECT };



#endif