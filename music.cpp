#include "Music.h"

void MusicMngr::UpdateMusic(){
	if (FeedinFlag == TRUE){
		if (nowPlayMusicVol == 0 && nextPlayMusic == ReturnMaxBGMVolume()){
			nowPlayMusic = nextPlayMusic;
			nextPlayMusic = -1;
			FeedinFlag = FALSE;
		}
		nowPlayMusicVol -= 5;
		nextPlayMusicVol += 5;
		if (nowPlayMusicVol < 0)nowPlayMusicVol = 0;
		if (nextPlayMusicVol > ReturnMaxBGMVolume())nextPlayMusicVol = ReturnMaxBGMVolume();
	}
	if (nowPlayMusic != -1)ChangeVolumeSoundMem(nowPlayMusic, nowPlayMusicVol);
	if (nextPlayMusic != -1)ChangeVolumeSoundMem(nextPlayMusic, nextPlayMusicVol);
	return;
}


int MusicMngr::PushMusic(int music_num, int feedinflag){
	StopSoundMem(nextPlayMusic);
	StopSoundMem(nowPlayMusic);
	nextPlayMusic = LS->ReturnSound(music_num);
	PlaySoundMem(nextPlayMusic, DX_PLAYTYPE_LOOP, TRUE);
	if (feedinflag == TRUE)
		FeedinFlag = TRUE;
	else
	if (feedinflag == FALSE)
		FeedinFlag = FALSE;
	return 0;
}

int MusicMngr::PushEffectSound(int music_num){
	PlaySoundMem(LS->ReturnEffectSound(music_num), DX_PLAYTYPE_BACK,TRUE);
	return 0;
}