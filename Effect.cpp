#include "Effect.h"
#include "Load.h"

WorpEffect::WorpEffect(int _x, int _y , int graphHandle){
	x = _x;
	y = _y;
	graph = graphHandle;
	count = 0;
	GraphNum = 0;
}

void WorpEffect::Draw(int _x , int _y){
	DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph , TRUE);
}

void WorpEffect::update(){
	count++;
	if (count % 7 == 0){
		GraphNum++;
		GraphNum = GraphNum % WORP_GRAPH_NUM;
		graph = LG->ReturnGraph(TypeEffect, Worp_P_Effect + GraphNum);
	}
}



ExploadEffect::ExploadEffect(int _x, int _y){
	Dim = 255;
	x = _x;
	y = _y;
	graph = LG->ReturnGraph(TypeEffect,Expload_Blue);
	count = 0;
	rad = 0.1f;
}

void ExploadEffect::Draw(int _x, int _y){
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Dim);/*���l��MAX�͂Q�T�T*/
	DrawRotaGraph(x - _x, y - _y, rad, 0.0f, graph, TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

void ExploadEffect::update(){
	rad += 0.1f;
	Dim -= 15;
	count++;
	if (count >= 17){
		SetFlag(FALSE);
	}
}




HitBlockEffect::HitBlockEffect(int _x, int _y, int color){
	Dim = 255;
	x = _x;
	y = _y;
	/*if (color == Effect_Color_BLUE){
		this->color = GetColor(25,135,22);
	}else 
	if (color == Effect_Color_WHITE){*/
		this->color = GetColor(250, 250, 250);
	/*}else{
		this->color = GetColor(0, 0, 0);
	}*/
	count = 0;
	rad = 0.1f;
}

void HitBlockEffect::Draw(int _x, int _y){
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Dim);/*���l��MAX�͂Q�T�T*/
	DrawCircle(x - _x, y - _y, rad, this->color, TRUE, TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

void HitBlockEffect::update(){
	rad += 1.5;
	Dim -= 15;
	count++;
	if (count >= 17){
		SetFlag(FALSE);
	}
}