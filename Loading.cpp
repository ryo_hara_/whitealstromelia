#include "SpecialScean.h"

void Loading::update(){
	DrawBox(-10,-10,800,800,GetColor(0,0,0),TRUE);
	//DrawFormatString(300,300,GetColor(0,0,0),"Loading");

	LG->LoadCharaStandingGraph();
	LG->LoadUI();
	LG->LoadStage();
	LG->LoadPlayerChara();
	LG->LoadEnemy();
	LG->LoadEffect();
	LG->LoadBullet();
	LG->LoadBackScrean();
	LG->LoadSceanGraph();
	LS->LoadBGM();
	SetFinishFlag(true);
	//WaitTimer(3000);
	return;
}