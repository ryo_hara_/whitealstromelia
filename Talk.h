#ifndef _TALK_H_
#define _TALK_H_

#include "Stage.h"

enum TALKCHAR{
	Talk_Null,
	Talk_Char1,
	Talk_Char2,
	Talk_Char3,
	Talk_Char4,
	Talk_Char1_2,
	Talk_Char1_3,
	Talk_Char2_3
};


class Talk :public scean{
private:
	int TalkPoint;
	int nowTalkChar;
	bool TalkSkipFlag;
public:
	Talk(){
		TalkSkipFlag = false;
		nowTalkChar = 0;
		TalkPoint = 0;
		SetNowState(State_Start);
		MMngr->PushMusic(TALK_BGM_1,TRUE);
	};
	InputMngr *IM = InputMngr::getInstance();
	LoadGraphic *LG = LoadGraphic::getInstance();
	MusicMngr *MMngr= MusicMngr::getInstance();
	DarkChange DC;
	void TalkSkip(){
		if (IM->key[TALKSKIP] % 10 == 1 && TalkSkipFlag == false){
			TalkSkipFlag = true;
			SetNowState(State_NextSceanAhead);
			DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
		}
	};
	int ReturnTalkPoint(){ return this->TalkPoint;}
	void SetTalkPoint(int num){ this->TalkPoint = num; };
	void AddTalkPoint(){ this->TalkPoint++; };
	void SetNowTalkChar(int TalkChar){ this->nowTalkChar = TalkChar; }
	int ReturnTalkChar(){ return this->nowTalkChar; };
	virtual void talk(){};
	virtual void Draw(){};
};


class FirstTalk :public Talk{
private:

public:
	FirstTalk(){
		IM->SetInputFlag(false);
		SetSceanType(Type_StartTalkScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		SetTalkPoint(-6);
	}
	void Draw();
	void talk();
	void update();
};


class Stage1_1to2TalkScean :public Talk{
private:

public:
	Stage1_1to2TalkScean(){
		IM->SetInputFlag(false);
		SetSceanType(Type_Stage1_1to2TalkScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
	}
	void Draw();
	void talk();
	void update();
};
class Stage1to2TalkScean :public Talk{
private:

public:
	Stage1to2TalkScean(){
		IM->SetInputFlag(false);
		SetSceanType(Type_Stage1to2TalkScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
	}
	void Draw();
	void talk();
	void update();
};

class Stage2_FirsttalkScean : public Talk{
private:
	Stage2_FirsttalkScean(){
		IM->SetInputFlag(false);
		SetSceanType(Type_Stage2_FirsttalkScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
	}
public:

	void Draw();
	void talk();
	void update();


};

class Stage2_1to2TalkScean :public Talk{
private:

public:
	Stage2_1to2TalkScean(){
		IM->SetInputFlag(false);
		SetSceanType(Type_Stage2_1to2TalkScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
	}
	void Draw();
	void talk();
	void update();
};

class Stage2_LastTalkScean :public Talk{
private:

public:
	Stage2_LastTalkScean(){
		IM->SetInputFlag(false);
		SetSceanType(Type_Stage2LastTalkScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
	}
	void Draw();
	void talk();
	void update();
};




enum TalkActiveChara{
	Active_Char1,
	Active_Char2,
	Active_Char3,
	Active_Char4,
	Active_Char1_2,
	Active_Char1_3,
	Active_Char1_4,
	Active_Char2_3,
};



#endif