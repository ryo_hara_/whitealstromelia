#include "Load.h"


/*���[�h�n�N���X(�V���O���g���N���X)�̃C���X�^���X*/
LoadGraphic* LoadGraphic::loadgraph = new LoadGraphic();
LoadSound* LoadSound::loadsound = new LoadSound();

int LoadGraphic::DeletAllGraph(){
	for (int i = 0; i < ENEMYGRAPH;i++)DeleteGraph(EnemyCharaGraph[i]);
	for (int i = 0; i < PCHARAGRAPH; i++)DeleteGraph(PCharaGraph[i]);
	for (int i = 0; i < MAXMAPCHIP; i++)DeleteGraph(MapChip[i]);
	for (int i = 0; i < TITLEGRAPH; i++)DeleteGraph(TitleGraph[i]);
	for (int i = 0; i < EFFECTGRAPH; i++)DeleteGraph(EffectGraph[i]);
	for (int i = 0; i < BULLETGRAPH; i++)DeleteGraph(BulletGraph[i]);
	for (int i = 0; i < SCEANGRAPH; i++)DeleteGraph(SceanGraph[i]);
	for (int i = 0; i < UIGRAPH; i++)DeleteGraph(UIGraph[i]);
	for (int i = 0; i < STANDINGFRAPH; i++)DeleteGraph(StandingCharaGraph[i]);
	for (int i = 0; i < BACKSCREANGRAPH; i++)DeleteGraph(BackScreanGraph[i]);
	return 0;
}

int LoadGraphic::ReturnGraph(int Type, int Allocate){
	if (Type == TypeMapChip){
		return MapChip[Allocate];
	}
	if (Type == TypePlayerChara){
		return PCharaGraph[Allocate];
	}
	if (Type == TypeEnemy){
		return EnemyCharaGraph[Allocate];
	}
	if (Type == TypeTitle){
		return TitleGraph[Allocate];
	}
	if (Type == TypeEffect){
		return EffectGraph[Allocate];
	}
	if (Type == TypeBullet){
		return BulletGraph[Allocate];
	}
	if (Type == TypeUI){
		return UIGraph[Allocate];
	}
	if (Type == TypeStandingGraph){
		return StandingCharaGraph[Allocate];
	}
	if (Type == TypeBackScreanGraph){
		return BackScreanGraph[Allocate];
	}
	if (Type == TypeSceanGraph){
		return SceanGraph[Allocate];
	}
	return -1;
}

int LoadGraphic::LoadUI(){
	if ((UIGraph[NUMERAL_TYPE1] = LoadGraph("Data\\�摜\\UI\\0.png")) == -1)return -1;
	UIGraph[NUMERAL_TYPE1 + 1]	= LoadGraph("Data\\�摜\\UI\\1.png");
	UIGraph[NUMERAL_TYPE1 + 2]	= LoadGraph("Data\\�摜\\UI\\2.png");
	UIGraph[NUMERAL_TYPE1 + 3]	= LoadGraph("Data\\�摜\\UI\\3.png");
	UIGraph[NUMERAL_TYPE1 + 4]	= LoadGraph("Data\\�摜\\UI\\4.png");
	UIGraph[NUMERAL_TYPE1 + 5]	= LoadGraph("Data\\�摜\\UI\\5.png");
	UIGraph[NUMERAL_TYPE1 + 6]	= LoadGraph("Data\\�摜\\UI\\6.png");
	UIGraph[NUMERAL_TYPE1 + 7]	= LoadGraph("Data\\�摜\\UI\\7.png");
	UIGraph[NUMERAL_TYPE1 + 8]	= LoadGraph("Data\\�摜\\UI\\8.png");
	UIGraph[NUMERAL_TYPE1 + 9]	= LoadGraph("Data\\�摜\\UI\\9.png");
	if ((UIGraph[MULTIPLY_TYPE1]= LoadGraph("Data\\�摜\\UI\\multiply.png")) == -1)return -1;
	if ((UIGraph[LIFEHERT]		= LoadGraph("Data\\�摜\\UI\\heart.png")) == -1)return -1;
	if ((UIGraph[LIFEFLAME]		= LoadGraph("Data\\�摜\\UI\\flame.png")) == -1)return -1;
	return 0;
}


int LoadGraphic::LoadCharaStandingGraph(){
	if ((StandingCharaGraph[BRAVE] = LoadGraph("Data\\�摜\\Standing\\Brave_p1.png")) == -1)return -1;
	if ((StandingCharaGraph[ARCHER] = LoadGraph("Data\\�摜\\Standing\\Archer_p.png")) == -1)return -1;
	if ((StandingCharaGraph[WITCH] = LoadGraph("Data\\�摜\\Standing\\Witch_p.png")) == -1)return -1;
	if ((StandingCharaGraph[DEVIL] = LoadGraph("Data\\�摜\\Standing\\�����e.png")) == -1)return -1;
	return 0;

}


int LoadGraphic::LoadTitle(){
	if ((TitleGraph[TMainGraph_0] = LoadGraph("Data\\�摜\\Title.png")) == -1)return -1;
	if ((TitleGraph[TMainGraph_1] = LoadGraph("Data\\�摜\\SceanGraph\\title_3.png")) == -1)return -1;
	if ((TitleGraph[StartCursol] = LoadGraph("Data\\�摜\\UI\\SwordIcon_1.png")) == -1)return -1;
	return 0;
}


int LoadGraphic::LoadStage(){
	if ((MapChip[GRASS] =	LoadGraph("Data\\�摜\\grass_1.png")) == -1)return -1;
	if ((MapChip[DESERTCHIP] = LoadGraph("Data\\�摜\\desert.png")) == -1)return -1;
	if ((MapChip[ROCK] =	LoadGraph("Data\\�摜\\rock_gray.png")) == -1)return -1;
	if ((MapChip[ROCK_RED] = LoadGraph("Data\\�摜\\rock_red.png")) == -1)return -1;
	if ((MapChip[BLOCK] =	LoadGraph("Data\\�摜\\block.png")) == -1)return -1;
	if ((MapChip[WORP_B] =	LoadGraph("Data\\�摜\\warp\\warp_blue.png")) == -1)return -1;
	if ((MapChip[WORP_G] = LoadGraph("Data\\�摜\\warp\\warp_green.png")) == -1)return -1;
	if ((MapChip[WORP_P] = LoadGraph("Data\\�摜\\warp\\warp_purple.png")) == -1)return -1;
	if ((MapChip[KEY_1] = LoadGraph("Data\\�摜\\MapObj\\key_1.png")) == -1)return -1;
	if ((MapChip[KEY_2] = LoadGraph("Data\\�摜\\MapObj\\key_2.png")) == -1)return -1;
	
	int door[4];
	LoadDivGraph("Data\\�摜\\MapObj\\gimmick.png", 4, 4, 1, 32, 32, door);
	for (int i = 0; i < 4; i++)MapChip[DOOR_1 + i] = door[i];

	return 1;
}


int LoadGraphic::LoadEffect(){
	if ((EffectGraph[Worp_B_Effect]	= LoadGraph("Data\\�摜\\warp\\warp_purple1.png")) == -1)return -1;
	EffectGraph[Worp_B_Effect + 1]	= LoadGraph("Data\\�摜\\warp\\warp_purple2.png");
	EffectGraph[Worp_B_Effect + 2]	= LoadGraph("Data\\�摜\\warp\\warp_purple3.png");
	EffectGraph[Worp_B_Effect + 3]	= LoadGraph("Data\\�摜\\warp\\warp_purple4.png");
	if ((EffectGraph[Worp_G_Effect] = LoadGraph("Data\\�摜\\warp\\warp_green1.png")) == -1)return -1;
	EffectGraph[Worp_G_Effect + 1]	= LoadGraph("Data\\�摜\\warp\\warp_green2.png");
	EffectGraph[Worp_G_Effect + 2]	= LoadGraph("Data\\�摜\\warp\\warp_green3.png");
	EffectGraph[Worp_G_Effect + 3]	= LoadGraph("Data\\�摜\\warp\\warp_green4.png");
	if ((EffectGraph[Worp_P_Effect] = LoadGraph("Data\\�摜\\warp\\warp_purple1.png")) == -1)return -1;
	EffectGraph[Worp_P_Effect + 1]	= LoadGraph("Data\\�摜\\warp\\warp_purple2.png");
	EffectGraph[Worp_P_Effect + 2]	= LoadGraph("Data\\�摜\\warp\\warp_purple3.png");
	EffectGraph[Worp_P_Effect + 3]	= LoadGraph("Data\\�摜\\warp\\warp_purple4.png");
	if ((EffectGraph[Expload_Blue]  = LoadGraph("Data\\�摜\\Effect\\Expload_Effect.png")) == -1)return -1;
	return 0;
}


int LoadGraphic::LoadPlayerChara(){
	int _graph[10];
	if ((PCharaGraph[TESTHERO] = LoadGraph("Data\\�摜\\hero.png")) == -1)return -1;
	if ((PCharaGraph[HERO_1] = LoadGraph("Data\\�摜\\Hero_1.png")) == -1)return -1;

	LoadDivGraph("Data\\�摜\\Hero_1_1.png",4,4,1,32,32,_graph);
	for (int i = 0; i < 4; i++)PCharaGraph[HERO_1 + i] = _graph[i];

	return 1;
}

int LoadGraphic::LoadEnemy(){
	int _graph[10];
	if ((EnemyCharaGraph[BAT] = LoadGraph("Data\\�摜\\enemy\\bat_0.png")) == -1)return -1;
	if ((EnemyCharaGraph[BIRD] = LoadGraph("Data\\�摜\\enemy\\bird_0.png")) == -1)return -1;
	if ((EnemyCharaGraph[DRAGON] = LoadGraph("Data\\�摜\\enemy\\dragon_0.png")) == -1)return -1;
	if ((EnemyCharaGraph[GHOST] = LoadGraph("Data\\�摜\\enemy\\ghost_0.png")) == -1)return -1;
	if ((EnemyCharaGraph[GOLEM] = LoadGraph("Data\\�摜\\enemy\\golem_0.png")) == -1)return -1;
	if ((EnemyCharaGraph[RABBIT] = LoadGraph("Data\\�摜\\enemy\\rabbit_0.png")) == -1)return -1;
	if ((EnemyCharaGraph[SLIME] = LoadGraph("Data\\�摜\\enemy\\slime_0.png")) == -1)return -1;
	LoadDivGraph("Data\\�摜\\enemy\\slime_1.png",4,4,1,32,32,_graph);
	for (int i = 0; i < 4; i++)EnemyCharaGraph[SLIME + i] = _graph[i];
	LoadDivGraph("Data\\�摜\\enemy\\rabbit_1.png", 4, 4, 1, 32, 32, _graph);
	for (int i = 0; i < 4; i++)EnemyCharaGraph[RABBIT + i] = _graph[i];
	LoadDivGraph("Data\\�摜\\enemy\\bird_1.png", 4, 4, 1, 32, 32, _graph);
	for (int i = 0; i < 4; i++)EnemyCharaGraph[BIRD + i] = _graph[i];
	LoadDivGraph("Data\\�摜\\enemy\\bat_1.png", 4, 4, 1, 32, 32, _graph);
	for (int i = 0; i < 4; i++)EnemyCharaGraph[BAT + i] = _graph[i];

	return 1;
}


int LoadGraphic::LoadBullet(){
	int _graph[10];
	if ((BulletGraph[RICE_B] = LoadGraph("Data\\�摜\\shot\\shot2.png")) == -1)return -1;
	LoadDivGraph("Data\\�摜\\shot\\hero_effect.png", 4, 4, 1, 32, 32, _graph);
	BulletGraph[SHOT_HERO_1] = _graph[1];
	if ((BulletGraph[RABBIT_BULLET] = LoadGraph("Data\\�摜\\shot\\RabbitBullet.png")) == -1)return -1;
	if ((BulletGraph[BAT_BULLET] = LoadGraph("Data\\�摜\\shot\\BatBullet.png")) == -1)return -1;
	if ((BulletGraph[BIRD_BULLET] = LoadGraph("Data\\�摜\\shot\\BirdBullet.png")) == -1)return -1;

	return 1;
}

int LoadGraphic::LoadBackScrean(){
	if ((BackScreanGraph[FOUNTAIN]	= LoadGraph("Data\\�摜\\SceanGraph\\fountain.png")) == -1)		return -1;
	if ((BackScreanGraph[GRASSLAND] = LoadGraph("Data\\�摜\\SceanGraph\\grass.png")) == -1)		return -1;
	if ((BackScreanGraph[WITCHHOUSE]= LoadGraph("Data\\�摜\\SceanGraph\\witch_house.png")) == -1)	return -1;
	if ((BackScreanGraph[DESERT] = LoadGraph("Data\\�摜\\SceanGraph\\desert.png")) == -1)	return -1;
	return 1;
};

int LoadGraphic::LoadSceanGraph(){
	if ((SceanGraph[GAMECLEAR] = LoadGraph("Data\\�摜\\SceanGraph\\clear_0.png")) == -1)		return -1;
	if ((SceanGraph[GAMECLEAR_S1] = LoadGraph("Data\\�摜\\SceanGraph\\5_Gameclear.png")) == -1)		return -1;
	if ((SceanGraph[GAMEOVER] = LoadGraph("Data\\�摜\\SceanGraph\\game_over_1.png")) == -1)	return -1;
	if ((SceanGraph[GAMEOVER_S1] = LoadGraph("Data\\�摜\\SceanGraph\\4_Gameover.png")) == -1)	return -1;
	return 1;
}



int LoadSound::DeletAllSound(){
	for (int i = 0; i <100; i++)DeleteSoundMem(BGM[i]);
	for (int i = 0; i <100; i++)DeleteSoundMem(EffectSound[i]);
	return 0;
}


int LoadSound::LoadBGM(){
	if ((BGM[TITLE_BGM]		= LoadSoundMem("Data\\���y\\titr.wav")) == -1)return -1;
	if ((BGM[TALK_BGM_1] = LoadSoundMem("Data\\���y\\stage_A.wav")) == -1)return -1;
	if ((BGM[STAGE_1_BGM]	= LoadSoundMem("Data\\���y\\stage_B.wav")) == -1)return -1;
	if ((BGM[GAMEOVER_BGM] = LoadSoundMem("Data\\���y\\tai.wav")) == -1)return -1;
	if ((BGM[CLEAR_BGM] = LoadSoundMem("Data\\���y\\test.wav")) == -1)return -1;

	LoadEffectSound();
	return 1;
}

int LoadSound::LoadEffectSound(){
	if ((EffectSound[ATTACK_SOUND_1]		= LoadSoundMem("Data\\���y\\���ʉ�\\slime1.wav")) == -1)return -1;
	if ((EffectSound[HIT_SOUND_1]			= LoadSoundMem("Data\\���y\\���ʉ�\\kick-low1.wav")) == -1)return -1;
	if ((EffectSound[PLAYER1_ATTACKSOUND]	= LoadSoundMem("Data\\���y\\���ʉ�\\sword-gesture1.wav")) == -1)return -1;
	if ((EffectSound[ENEMY_HITSOUND_1]		= LoadSoundMem("Data\\���y\\���ʉ�\\katana-clash4.wav")) == -1)return -1;
	if ((EffectSound[CURSOL_SOUND_1]		= LoadSoundMem("Data\\���y\\���ʉ�\\cursor7.wav")) == -1)return -1;
	if ((EffectSound[DECISION_SOUND_1]		= LoadSoundMem("Data\\���y\\���ʉ�\\decision1.wav")) == -1)return -1;
	return 1;
}


int LoadSound::ReturnSound(int Allocation){
	return BGM[Allocation];
}

int LoadSound::ReturnEffectSound(int Allocation){
	return EffectSound[Allocation];
}