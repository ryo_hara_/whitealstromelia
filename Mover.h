#ifndef _MOVER_H_
#define _MOVER_H_

#include "Load.h"
#include "InputMngr.h"
#include "Music.h"
#include "Calculate.h"


#define MAGNIFICATION 1.5f
#define MAPCHIPSIZE 96*MAGNIFICATION
#define MAPOBJECTSIZE 32*MAGNIFICATION

enum{
	TYPE_RECTANGLE, TYPE_CIRCLE
};

enum ObjType{
	ObjType_Map_Block,
	ObjType_Item_Key,
	ObjType_Map_Door
};


class actor{
private:
	int flag;
	int CollisionDetectionFlag;//当たり判定の付与を決めるフラグ変数
public:
	actor(){
		this->collision_type			= TYPE_RECTANGLE;
		this->count						= 0;
		this->flag						= 1;
		this->graphAllocat				= 0;
		this->rad						= 0;
		this->alpha						= 255;
		this->CollisionDetectionFlag	= TRUE;
	}
	double x, y;
	double vectorx, vectory;						//当たり判定様に移動方向を格納する変数
	int sizex, sizey;
	int collision_type;
	int graph;
	int graphAllocat;								//元の画像から何枚目の画像を呼び出すか加算する変数(差分用)
	int count;
	double rad;
	int alpha;										//アルファ値
	LoadGraphic *LG = LoadGraphic::getInstance();
	virtual void Draw(int x, int y) = 0;
	virtual void update()=0;
	int		SetFlag(int _flag)					{ flag = _flag; return flag;}
	int		ReturnFlag()						{ return flag; }
	void	SetCollisionDetectionFlag(int flag)	{ CollisionDetectionFlag = flag; }//当たり判定の付与を決める変数のセット
	int		ReturnCollisionDetectionFlag()		{ return CollisionDetectionFlag; }//当たり判定の付与を決める変数を返す関数
};


class Enemy :public actor{
private:
	int AttackFlag;
	int HitFlag;
	int HP;
	int EnemyType;
public:
	int Hittimecounter;

	MusicMngr	*MMngr = MusicMngr::getInstance();
	Enemy(){ HitFlag = FALSE; Hittimecounter = 0; HP = 2;}
	int		SetAttackFlag(int n)			{ AttackFlag = n; return AttackFlag; }
	int		ReturnAttackFlag()				{return AttackFlag;}
	void	SetHitFlag(int flag, int Damage){ HitFlag = flag; };
	int		ReturnHitFlag()					{ return HitFlag; };
	void	SubHP(int HitPoint)				{ HP -= HitPoint; };
	int		ReturnHP()						{ return HP; };
	virtual int Hit()						{ return 0; };

	void setEnemyType(int type){ this->EnemyType = type; }
	int getEnemyType(){ return this->EnemyType; }
};


class MapObj :public actor{
private:
	int objtype;
public:
	void setObjType(int objtype){ this->objtype = objtype; return; }
	int getObjType(void){ return this->objtype; }
};

class Effect :public actor{
	public:
		int Dim;
};


class Bullet :public actor{
private:
	int BulletType;
	int AttackPoint;
	bool quarterDirectionFlag;
public:
	calculate Cal;
	Bullet(){ AttackPoint = 1;}
	int		SetBulletType(int n)	{ BulletType = n; return n; }
	int		ReturnBulletType()		{ return BulletType; };
	void	SetAttckPoint(int AP)	{ AttackPoint = AP; };
	int		ReturnAttackPoint()		{ return AttackPoint; }

	void setQuarterDirectionFlag(bool a){ quarterDirectionFlag = a; }
	bool getQuarterDirectionFlag(){ return quarterDirectionFlag;}
};

enum BulletType{
	ENEMY_BULLET,
	PLAYER_BULLET
};


#endif


