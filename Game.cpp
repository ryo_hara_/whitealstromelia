#include "Game.h"
#include "Load.h"
#include "SpecialScean.h"
#include "Talk.h"
#include <time.h>



int Game::gamemain(){
	SetMainWindowText("GAME");
	ChangeWindowMode(TRUE);
	SetGraphMode(WINDOWSIZEX, WINDOWSIZEY, 32);
	if (DxLib_Init() == -1) return -1;
	srand((unsigned int)time(0));
	Loading L;
	L.update();
	PushScean(new Title(), BACK);
	//PushScean(new Stage2_1,BACK);
	//PushScean(new Title,BACK);
	//PushScean(new Loading, BACK);
	//PushScean(new Stage1_1,BACK);
	//PushScean(new StageFlame,BACK);
	//PushScean(new GameOver, BACK)
	//PushScean(new StageFlame,BACK);
	//PushScean(new FirstTalk,BACK);
	//PushScean(new Stage1to2TalkScean,BACK);
	//PushScean(new Stage1_2,BACK);
	//PushScean(new StageFlame,BACK);
	//PushScean(new Clear_Trial,BACK);
	while (ProcessMessage() == 0 && ClearDrawScreen() == 0 &&  IM->GetInput() == 0 ){
		for (std::list<scean*>::iterator itr = Sceanmngr.begin(); itr != Sceanmngr.end(); ){
			if ((*itr)->ReturnUpdateFlag() == true){
				(*itr)->update();
				(*itr)->UpdateList();
			}
			if ((*itr)->ReturnFinishFlag() == true){
				int n = TransScean((*itr)->RetrunSceanType());
				if (n == -1)exit(0);
				itr = Sceanmngr.erase(itr);
				continue;
			}
			itr++;
		}
		if (ScreenFlip() != 0)break;
	}
	return 0;
}




int Game::PushScean(scean* s , int type){
	if (s == NULL)return -1;
	if (type == BACK)
		Sceanmngr.push_back(s);
	else
	if (type == FRONT)
		Sceanmngr.push_front(s);
	return 1;
}


int Game::DeleteScean(std::list<scean*>::iterator *it){
	if (it == NULL)return -1;
	*it = Sceanmngr.erase(*it);
	return 1;
}





void Game::update(){
	return;
}

//ステージ切替時の暗転処理などは上位で定義
int Game::TransScean(int _Type){//終了するシーンを引数に取り、次のシーンをイテレーターにプッシュする処理
	if (PM->ReturnPlayerGameOverFlag() == true){
		PushScean(new GameOver,BACK);
		return 1;
	}
	switch (_Type){
	case Type_Flame:
		break;
	case Type_Title:
		PushScean(new Loading(),BACK);
		break;
	case Type_StartLoading:
		PushScean(new FirstTalk,BACK);
		break;
	case Type_StartTalkScean:
		PushScean(new Stage1_1, BACK);
		PushScean(new StageFlame, BACK);
		break;
	case Type_Stage1_1:
		PushScean(new Stage1_1to2TalkScean, BACK);
		break;
	case Type_Stage1_1to2TalkScean:
		PushScean(new Stage1_2, FRONT);
		PushScean(new StageFlame, BACK);

		break;
	case Type_Stage1_2:
		PushScean(new Stage1to2TalkScean,BACK);
		break;
	case Type_Stage1to2TalkScean:
		PushScean(new Stage2_1, FRONT);
		PushScean(new StageFlame, BACK);
		break;
	case Type_Stage2_1:
		PushScean(new Stage2_1to2TalkScean, BACK);
		//PushScean(new Stage2_2, FRONT);
		break;
	case Type_Stage2_1to2TalkScean:
		PushScean(new Stage2_2, FRONT);
		PushScean(new StageFlame, BACK);
		break;
	case Type_Stage2_2:
		PushScean(new Stage2_LastTalkScean, BACK);
		break;
	case Type_Stage2LastTalkScean:
		PushScean(new Clear_Trial, BACK);
		break;



	case Type_Stage2to3TalkScean:
		PushScean(new Clear_Trial, FRONT);
		break;
	case Type_TrialVerFinishScean:
		PushScean(new Title, FRONT);
		break;
	case Type_GameOver:
		PushScean(new Title, FRONT);
		break;
	default:
		return -1;
	}

	return 1;
}

/*
void Game::ManageScean(std::list<scean*>::iterator itr){
		if ((*itr)->ReturnFinishFlag() == true){
			DeleteScean(&itr);
			//PushScean(new Stage1_1);
		}
	

	if (nowState == TITLE){

	}
}*/

