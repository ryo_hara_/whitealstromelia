#include "Talk.h"


void Stage1to2TalkScean::update(){
	TalkSkip();
	talk();
	if (ReturnNowState() == State_Start){//現在の状態がState_Startならフェードイン処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetNowState(State_Update);
		}
	}
	else
	if (ReturnNowState() == State_Update){//現在の状態がState_Updateなら通常処理
		IM->SetInputFlag(true);
	}
	else
	if (ReturnNowState() == State_NextSceanAhead){//現在の状態がState_NextSceanAheadならフェードアウト処理
		DC.update();
		if (DC.ReturnDarkChengeFlag() == DARKCHENGE_Type_Null){
			SetFinishFlag(true);
		}
	}

}

void Stage1to2TalkScean::talk(){
	Draw();
	static int Char1Dim = 135;
	static int Char2Dim = 0;
	static int Char3Dim = 0;
	//背景
	SetDrawMode(DX_DRAWMODE_BILINEAR);
	
	if (ReturnTalkPoint() >= 46 && 50 >= ReturnTalkPoint()){ 
		DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, FOUNTAIN), TRUE);
	}else{
		DrawRotaGraph(400, 300, 1.0f, 0.0f, LG->ReturnGraph(TypeBackScreanGraph, GRASSLAND), TRUE);
	}

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char1Dim);
	DrawRotaGraph(150, 600, 1.0f, 0.0f, LG->ReturnGraph(TypeStandingGraph, BRAVE), TRUE);//Char1
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char2Dim);
	DrawRotaGraph(650, 600, 0.95f, 0.0f, LG->ReturnGraph(TypeStandingGraph, ARCHER), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, Char3Dim);
	DrawRotaGraph(400, 600, 0.90f, 0.0f, LG->ReturnGraph(TypeStandingGraph, WITCH), TRUE);//Char2
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);

	if (46 <= ReturnTalkPoint() && ReturnTalkPoint() <= 50){
		SetDrawMode(DX_DRAWMODE_BILINEAR);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 135);
		DrawRotaGraph(650, 600-100, 0.90f, 0.0f, LG->ReturnGraph(TypeStandingGraph, DEVIL), TRUE);//Char2
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		SetDrawMode(DX_DRAWMODE_NEAREST);
	}

	SetDrawMode(DX_DRAWMODE_BILINEAR);//線形補間関数
	//テキストボックス
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 200);
	DrawBox(50, 450, 750, 575, GetColor(0, 0, 0), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	SetDrawMode(DX_DRAWMODE_NEAREST);


	if (ReturnTalkPoint() == 91){
		SetNowState(State_NextSceanAhead);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedout);
	}


	switch (ReturnTalkPoint()){
	case 0:
		Char1Dim = 135;
		Char2Dim = 0;
		Char3Dim = 0;
		SetFontSize(32);
		DrawString(70, 510, "−草原−", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 1:
		SetFontSize(24);
		DrawString(70, 470, "　見渡す限り広がっていた緑が少しずつ減っていき乾燥し", GetColor(235, 235, 235));
		DrawString(70, 500, "た大地が目に付くようになってきている。勇者一行は草原", GetColor(235, 235, 235));
		DrawString(70, 530, "と砂漠の境目、その少し手前で腰を下ろして休んでいた。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	case 2:
		DrawString(70, 470, "「やっと着いたなぁ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 3:
		DrawString(70, 470, "「思いのほか疲れたな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 4:
		DrawString(70, 470, "「・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 5:
		DrawString(70, 470, "「取りあえず今日はここまでだなぁ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 6:
		DrawString(70, 470, "「まぁ、これ以上は無理だろうな（主にローズが）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 7:
		DrawString(70, 470, "「・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 8:
		DrawString(70, 470, "「まぁ、特に何事もなく着けて良かった（ローズ以外は）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 9:
		DrawString(70, 470, "「嗚呼、そうだな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 10:
		DrawString(70, 470, "「・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 11:
		DrawString(70, 470, "「（死んでる？）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 12:
		DrawString(70, 470, "「（ああ、そうらしいな）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 13:
		DrawString(70, 470, "「（どうする？）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 14:
		DrawString(70, 470, "「（取りあえず無視しとけばいいだろ）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 15:
		DrawString(70, 470, "「（それでいいのか？）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 16:
		DrawString(70, 470, "「（どうせ後で目を覚ますさ）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 17:
		DrawString(70, 470, "「まぁ大丈夫か。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 18:
		DrawString(70, 470, "「さぁ、飯の準備でもするか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 19:
		SetFontSize(32);
		DrawString(70, 510, "−時間経過中−", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 20:
		SetFontSize(24);
		DrawString(70, 470, "パチパチと薪のはじける音が周囲に響く中、３人はたき火", GetColor(235, 235, 235));
		DrawString(70, 500, "を囲むようにして座りながら食事をとり、談笑していた。", GetColor(235, 235, 235));
		DrawString(70, 530, "なお約一名に関しては調理中に復活したようである。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 21:
		DrawString(70, 470, "「二人ともさぁ、無視するとか酷くない？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 22:
		DrawString(70, 470, "「まだ言ってるのか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 23:
		DrawString(70, 470, "「おかわりいる？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 24:
		SetFontSize(32);
		DrawString(70, 470, "「いる。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 25:
		SetFontSize(24);
		DrawString(70, 470, "「俺もくれ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 26:
		DrawString(70, 470, "「はいよ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 27:
		DrawString(70, 470, "「本当、料理上手だよねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 28:
		DrawString(70, 470, "「ああ、本当にな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 29:
		DrawString(70, 470, "「ローズはもう少し家事とかしろよ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 30:
		DrawString(70, 470, "「面倒なんだもん。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 31:
		DrawString(70, 470, "「面倒なのは分かるがな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 32:
		DrawString(70, 470, "「そういえばさぁ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

/********************************************************************************/
	case 33:
		DrawString(70, 470, "「何？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 34:
		DrawString(70, 470, "「何で魔王の城目指してるんだっけ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 35:
		DrawString(70, 470, "「今更！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 36:
		DrawString(70, 470, "「お前なぁ、そんなことも忘れたのか？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 37:
		DrawString(70, 470, "「そうだ、言ってやれアゲート。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 38:
		DrawString(70, 470, "「・・・・・・何だっけ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 39:
		DrawString(70, 470, "「お前もかよ！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 40:
		DrawString(70, 470, "「だってねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 41:
		DrawString(70, 470, "「そうだな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 42:
		DrawString(70, 470, "「正直そこまで興味なかったから。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2_3);
		break;
	case 43:
		DrawString(70, 470, "「マジかよ・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 44:
		DrawString(70, 470, "「という訳で説明プリーズ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 45:
		DrawString(70, 470, "「何だかなぁ。まぁいいけどさ、あれは一か月前の事なん", GetColor(235, 235, 235));
		DrawString(70, 500, "  だけど・・・・・・。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;


	case 46:
		Char1Dim = 0;
		Char2Dim = 0;
		Char3Dim = 0;
		SetFontSize(32);
		DrawString(70, 480, "−王都−", GetColor(235, 235, 235));
		DrawString(70, 520, "一か月前", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 47:
		SetFontSize(24);
		DrawString(70, 470, "あの日俺は特に何かをするわけでもなく噴水の所にいたん", GetColor(235, 235, 235));
		DrawString(70, 500, "だ。ぼうっとしながら周りを見ていて、いつもと変わらな", GetColor(235, 235, 235));
		DrawString(70, 530, "い人の多さと賑やかさだった。するとある一点が目に付い", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 48:
		DrawString(70, 470, "たんだ。そこだけ人だかりができていてさ、中心に誰かい", GetColor(235, 235, 235));
		DrawString(70, 500, "るらしくて気になって俺も見に行ったんだよ。そして、そ", GetColor(235, 235, 235));
		DrawString(70, 530, "の中心にいた人を見た瞬間俺は悟ったんだ、これは運命だ", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 49:
		DrawString(70, 470, "って、そして今日はまさしく運命の結実とも呼べる日だっ", GetColor(235, 235, 235));
		DrawString(70, 500, "たと。気付けば俺はその人に対して傅いてこう言っていた", GetColor(235, 235, 235));
		DrawString(70, 530, "のさ。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 50:
		DrawString(70, 470, "「結婚してください。」", GetColor(235, 235, 235));
		DrawString(70, 500, "ってね・・・・・・。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;



	case 51:
		Char1Dim = 135;
		Char2Dim = 135;
		Char3Dim = 135;
		DrawString(70, 470, "「成程、まぁお前が俺以上に馬鹿だってことは分かった。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 52:
		DrawString(70, 470, "「ロマンスだねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 53:
		DrawString(70, 470, "「で、それがどうしたらこうなるんだ？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;


	case 54:
		DrawString(70, 470, "「いや、何かねぇその後話したらその人が実はねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 55:
		DrawString(70, 470, "「何だ、まさか魔王だったとか言わないよな。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 56:
		DrawString(70, 470, "「それはないでしょー。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 57:
		DrawString(70, 470, "「あ、その通り。よく分かったね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 58:
		DrawString(70, 470, "「(�Vσ�V)」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 59:
		DrawString(70, 470, "「(*ﾟρﾟ)」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 60:
		DrawString(70, 470, "「いや、本当に驚いたよ。まさか魔王だなんて思わないよ", GetColor(235, 235, 235));
		DrawString(70, 500, "　ね。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 61:
		DrawString(70, 470, "「・・・・・・」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 62:
		SetFontSize(24);
		DrawString(70, 470, "「・・・・・・」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 63:
		DrawString(70, 470, "「最初は半信半疑だったけどさぁ、お付の魔物とかいて本", GetColor(235, 235, 235));
		DrawString(70, 500, "　当だって分かってね。まぁ色々あって、結局『弱いやつ", GetColor(235, 235, 235));
		DrawString(70, 530, "  と結婚する気はない！！』って言われて。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 64:
		DrawString(70, 470, "「」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 65:
		DrawString(70, 470, "「」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;


	case 66:
		DrawString(70, 470, "「その後に『本気だというのなら私の用意する試練を乗り", GetColor(235, 235, 235));
		DrawString(70, 500, "  越えて城まで来いっ！！』ってことになって。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 67:
		DrawString(70, 470, "「・・・・・・ああそれで。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 68:
		DrawString(70, 470, "「・・・・・・そういうこと。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 69:
		DrawString(70, 470, "「どうしたの？そんな疲れた顔して。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 70:
		DrawString(70, 470, "「お前のせいだよ・・・・・・」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 71:
		DrawString(70, 470, "「（どーすんのさこれ）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 72:
		DrawString(70, 470, "「（どーしようもないだろ、もう巻き込まれてるし）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;
	case 73:
		DrawString(70, 470, "「まぁいいや、でスライムとかうさぎが襲ってきてたのは", GetColor(235, 235, 235));
		DrawString(70, 500, "　それが試練の内容だからだって。さっき倒したスライム", GetColor(235, 235, 235));
		DrawString(70, 530, "　さんが言ってた。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 74:
		DrawString(70, 470, "「確かに何で襲ってくるのか不思議だったけどさぁ。（ま", GetColor(235, 235, 235));
		DrawString(70, 500, "  あ面倒だからどうでもいいか）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 75:
		DrawString(70, 470, "「成程、そうだったのか（時すでに遅し、まぁなるように", GetColor(235, 235, 235));
		DrawString(70, 500, "  なるか）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 76:
		DrawString(70, 470, "「いや、でも本当に一目ぼれってあるんだねぇ。いやぁ本", GetColor(235, 235, 235));
		DrawString(70, 500, "　当に綺麗な人でねえ、いや可愛いかな？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 77:
		DrawString(70, 470, "「・・・・・・寝るか。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 78:
		DrawString(70, 470, "「そーだね、疲れたよもう。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;

	case 79:
		DrawString(70, 470, "そう言ってそそくさと二人は寝る準備をし始めた。その顔", GetColor(235, 235, 235));
		DrawString(70, 500, "は幾分がげっそりとし疲労の色が濃く見える。この短時間", GetColor(235, 235, 235));
		DrawString(70, 530, "ですごい変わりようだ。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 80:
		DrawString(70, 470, "「ちょっと待ってよ二人とも！！」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 81:
		DrawString(70, 470, "「ほらお前も手伝え。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 82:
		DrawString(70, 470, "「もう少し話聞いてくれてもよくない！？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 83:
		DrawString(70, 470, "「早く準備してよー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char3);
		break;
	case 84:
		DrawString(70, 470, "「面倒になってやめたー」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;
	case 85:
		DrawString(70, 470, "「さっさとしろ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2);
		break;

	case 86:
		DrawString(70, 470, "「何か急に冷たくない？」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 87:
		DrawString(70, 470, "「気のせいだ（だよ）」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char2_3);
		break;

	case 88:
		DrawString(70, 470, "「納得いかねぇ。」", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Char1);
		break;

	case 89:
		DrawString(70, 470, "結局、ローズの分も準備をし始めたラズ、こんな調子で大", GetColor(235, 235, 235));
		DrawString(70, 500, "丈夫なのだろうか？まだまだ旅は始まったばかり、果たし", GetColor(235, 235, 235));
		DrawString(70, 530, "てラズの恋は実るのだろうか？そもそも無事に魔王城まで", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;
	case 90:
		DrawString(70, 470, "辿り着けるのだろうか？勇者一行の旅はまだ始まったばか", GetColor(235, 235, 235));
		DrawString(70, 500, "りである。", GetColor(235, 235, 235));
		SetNowTalkChar(Talk_Null);
		break;

	}


	if (IM->key[ATTACK] % 10 == 1)AddTalkPoint();


	switch (ReturnTalkChar()){
	case Talk_Null:
		Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char1:
		Char1Dim = 255;
		if (Char2Dim != 0)Char2Dim = 135;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char2:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		if (Char3Dim != 0)Char3Dim = 135;
		break;
	case Talk_Char3:
		if (Char1Dim != 0)Char1Dim = 135;
		if (Char2Dim != 0)Char2Dim = 135;
		Char3Dim = 255;
		break;
	case Talk_Char2_3:
		if (Char1Dim != 0)Char1Dim = 135;
		Char2Dim = 255;
		Char3Dim = 255;
		break;
	}


}

void Stage1to2TalkScean::Draw(){
}