#ifndef _GAME_H_
#define _GAME_H_

#include <List>
#include "Stage.h"
#include "InputMngr.h"

#define WINDOWSIZEX 800
#define WINDOWSIZEY 600



class Game{
private:
	int nowState;
	Game(){ 
	};
	~Game(){};
public:
	std::list<scean*> Sceanmngr;
	static Game* getInstance(){ 
		static Game * game = new Game();
		return game; 
	}
	InputMngr *IM = InputMngr::getInstance();
	PlayerMngr *PM = PlayerMngr::getInstance();
	LoadGraphic *LG = LoadGraphic::getInstance();

	int PushScean(scean *s , int PushType);
	int DeleteScean(std::list<scean*>::iterator *it);
	int gamemain(void);


	int ActiveStage;

	void update(void);
	void NextScean(void);//次のシーンを追加するための関数
	//タいトルの終了フラグが立つ→遷移処理(暗転)→Stage1＿１追加
	int TransScean(int _Type);//あるシーンの終了処理直前に次のシーンを追加する処理


	enum PushType{
		FRONT, BACK
	};
};








#endif