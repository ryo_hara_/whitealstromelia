#include "MapObj.h"
#include "Stage.h"
#include "Game.h"

Rock::Rock(int x_ , int y_,int grphandle){
	sizex = MAPOBJECTSIZE;
	sizey = MAPOBJECTSIZE;
	x = x_;
	y = y_;
	graph = grphandle;
}

void Rock::Draw(int _x, int _y){
	DrawRotaGraph(x - _x, y - _y, MAGNIFICATION*1.3f, 0.0f, graph, TRUE);
}

void Rock::update(){

}

WorpPoint::WorpPoint(int x_, int y_, int grphandle){
	sizex = MAPOBJECTSIZE;
	sizey = MAPOBJECTSIZE;
	x = x_;
	y = y_;
	graph = grphandle;
	SetCollisionDetectionFlag(FALSE);
}

void WorpPoint::Draw(int _x,int _y){
	DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
}

void WorpPoint::update(){

}

Key_1::Key_1(int x_, int y_, int grphandle){
	sizex = MAPOBJECTSIZE;
	sizey = MAPOBJECTSIZE;
	x = x_;
	y = y_;
	graph = grphandle;
	SetCollisionDetectionFlag(FALSE);
	setObjType(ObjType_Item_Key);
}

void Key_1::Draw(int _x, int _y){
	DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
}

void Key_1::update(){

}

Door::Door(int _x, int _y, int grphandle){
	sizex = MAPOBJECTSIZE;
	sizey = MAPOBJECTSIZE;
	x = _x;
	y = _y;
	graph = grphandle;
	SetCollisionDetectionFlag(TRUE);
	setObjType(ObjType_Map_Door);
}

void Door::update(){

}

void Door::Draw(int _x, int _y){
	DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, 0.0f, graph, TRUE);
}