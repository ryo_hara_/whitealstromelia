#include "Bullet.h"
#define PI 3.1415

void straightBullet::update(){
	x += vectorx;
	y += vectory;
}

void straightBullet::Draw(int _x , int _y){
	DrawRotaGraph(x - _x, y - _y, MAGNIFICATION, rad+PI/2, graph, TRUE);
}


void Player1Bullet::update(){
	x += vectorx;
	y += vectory;
}

void Player1Bullet::Draw(int _x, int _y){
	DrawRotaGraph(x - _x, y - _y, 1.0f, rad+PI/2, graph, TRUE);
}


void RabbitBullet::update(){
	x += vectorx;
	y += vectory;
}

void RabbitBullet::Draw(int _x , int _y){
	DrawRotaGraph(x - _x, y - _y, 0.5f, rad + PI / 2, graph, TRUE);

}

void BirdBullet::update(){
	x += vectorx;
	y += vectory;
}

void BirdBullet::Draw(int _x , int _y){
	DrawRotaGraph(x - _x, y - _y, 0.5f, rad + PI / 2, graph, TRUE);
}


void BatBullet::update(){
	x += vectorx;
	y += vectory;
}

void BatBullet::Draw(int _x, int _y){
	DrawRotaGraph(x - _x, y - _y, 0.5f, rad + PI / 2 + PI, graph, TRUE);
}