#ifndef _SPECIALSCEAN_H_
#define _SPECIALSCEAN_H_

#include "Stage.h"
#include "Music.h"
#include "Calculate.h"

class StageFlame:public scean{
public:
	StageFlame(){
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		SetSceanType(Type_Flame);
	};
	//void setFeedIn(bool a){if(a == true)}
	LoadGraphic *LG = LoadGraphic::getInstance();
	PlayerMngr	*PM = PlayerMngr::getInstance();
	void update();
	void draw();
};

enum TITLECURSOLTYPE{
	Cursol_GameStart,
	Cursol_Exit
};


class Title :public scean{
private:
	double CursolObj;
	int nowCursol;
public:
	LoadGraphic *LG		= LoadGraphic::getInstance();
	LoadSound	*LS		= LoadSound::getInstance();
	InputMngr	*IM		= InputMngr::getInstance();
	MusicMngr	*MMngr	= MusicMngr::getInstance();
	PlayerMngr	*PM		= PlayerMngr::getInstance();

	Title(){
		LG->DeletAllGraph();
		LS->DeletAllSound();
		PM->Initialize();
		SetUpdateFlag(true);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		LG->LoadTitle();
		LS->LoadBGM();
		SetSceanType(Type_Title);
		SetNowState(State_Start);
		CursolObj = 0.0f;
		nowCursol = Cursol_GameStart;
	};
	~Title(){};
	void update();
	void draw();
	void UpdateList();
};




class Transition :public scean{
	void update();
	void draw();
};


class Loading : public scean{
public:
	LoadGraphic *LG = LoadGraphic::getInstance();
	LoadSound	*LS = LoadSound::getInstance();

	Loading(){
		SetSceanType(Type_StartLoading);
	};
	void update();
	void draw(){};
	void UpdateList(){};
};


class Clear_Trial : public scean{
private:

public:
	InputMngr	*IM		= InputMngr::getInstance();
	LoadGraphic *LG		= LoadGraphic::getInstance();
	LoadSound	*LS		= LoadSound::getInstance();
	PlayerMngr	*PM		= PlayerMngr::getInstance();
	MusicMngr	*MMngr	= MusicMngr::getInstance();
	Clear_Trial(){
		SetSceanType(Type_TrialVerFinishScean);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		PM->Initialize();
		MMngr->PushMusic(CLEAR_BGM, TRUE);
	}
	void update();
	void draw();
	void UpdateList(){};
};

class GameOver : public scean{
private:
public:
	InputMngr	*IM		= InputMngr::getInstance();
	LoadGraphic *LG		= LoadGraphic::getInstance();
	LoadSound	*LS		= LoadSound::getInstance();
	PlayerMngr	*PM		= PlayerMngr::getInstance();
	MusicMngr	*MMngr	= MusicMngr::getInstance();
	GameOver(){
		SetSceanType(Type_GameOver);
		SetNowState(State_Start);
		DC.SetDarkChenge(DARKCHENGE_Type_Feedin);
		PM->Initialize();
		MMngr->PushMusic(GAMEOVER_BGM, TRUE);
	}
	void update();
	void draw();
	void UpdateList(){};
};

#endif